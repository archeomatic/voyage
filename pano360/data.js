// ici les données qui servent de parametre dans index.js
var APP_DATA = {
  // création des scenes
  "scenes": [
    //scene 1: la cuisine
	{
      "id": "0-cuisine",
      "name": "Cuisine",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 1024,
      "initialViewParameters": {
		// rotation horizontale de la tête (droite/gauche)
        "yaw": 0.04905924000000361,
		// rotation verticale de la tête (haut/bas)
        "pitch": 0,
		// field of vision (champ de vision)
        "fov": 1.4811428556961166
      },
	  // coordonnées d'un hotspot de lien
      "linkHotspots": [
        {
          "yaw": -2.6862876934455997,
          "pitch": 0.6178032110738574,
          "rotation": 6.283185307179586,
          "target": "2-sjour---salle--manger"
        }
      ],
	  // coordonnées et contenu d'un hotspot d'information
      "infoHotspots": [
        {
          "yaw": 1.3429853500531443,
          "pitch": 0.7044484403325271,
          "title": "Lapin-garoup",
          "text": "en manque de graine<br>"
        }
      ]
    },
	//fin de la scene 1
	//scene 2: le salon
    {
      "id": "1-salon",
      "name": "Salon",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 1024,
      "initialViewParameters": {
        "yaw": -1.598394247179817,
        "pitch": 0,
        "fov": 1.4811428556961166
      },
      "linkHotspots": [
        {
          "yaw": 0.04558451229921978,
          "pitch": 0.5968200627003863,
          "rotation": 0,
          "target": "2-sjour---salle--manger"
        }
      ],
      "infoHotspots": [
        {
          "yaw": 1.7979488736414648,
          "pitch": 0.1762830002663165,
          "title": "Le chat<br>",
          "text": "n'est pas à vendre..<br>"
        }
      ]
    },
	//fin de la scene 2
	//scene 3: le séjour - sam
    {
      "id": "2-sjour---salle--manger",
      "name": "Séjour - Salle à manger",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 1024,
      "initialViewParameters": {
        "yaw": 1.5621139974557288,
        "pitch": 0.17346080542023934,
        "fov": 1.4811428556961166
      },
      "linkHotspots": [
        {
          "yaw": 0.08017318047514088,
          "pitch": 0.6467396366895848,
          "rotation": 6.283185307179586,
          "target": "0-cuisine"
        },
        {
          "yaw": 2.811754456285236,
          "pitch": 0.6650573835511473,
          "rotation": 6.283185307179586,
          "target": "1-salon"
        }
      ],
      "infoHotspots": [
        {
          "yaw": -2.6714214731070687,
          "pitch": 0.6010406391778211,
          "title": "Sapin",
          "text": "Bah oui... c'est noël !<br>"
        },
        {
          "yaw": 1.353224538207762,
          "pitch": -0.15510769339348052,
          "title": "Bornes d'arcade<br>",
          "text": "A vendre: https://docs.google.com/document/d/1pTz4lqRtMcERAi04yEzoUIVik1-s-wsSNv-B7WbibnY/edit?ts=592eac06<br>"
        }
      ]
    }
	//fin de la scene 3
  ],
  //fin des scenes
  "name": "Visite Ma Maison en 360°",
  "settings": {
	// mode de vue "drag" ou ?"qtvr"
    "mouseViewMode": "drag",
	// rotation auto de la scene
    "autorotateEnabled": true,
	// affichage bouton plein écran
    "fullscreenButton": true,
	// affichage boutons de controle
    "viewControlButtons": false
  }
};
