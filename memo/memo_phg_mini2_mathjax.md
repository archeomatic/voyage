# Photogrammétrie avec un drone DJI mini 2

L'usage de la photogrammétrie par drone en archéologie pourrait être divisé en 2 grands objectifs:

test math:
$a^2+b^2=c^2 $
ou 
test math:
$$a^2+b^2=c^2$$
ou
\$\$ a^2+b^2=c^2 \$\$
ou
\$ a^2+b^2=c^2 \$
ou 
\$\$a^2+b^2=c^2\$\$
ou
\$a^2+b^2=c^2\$


```math
a^2+b^2=c^2
```

ou plutôt en mathjax:

\\[ a^2+b^2=c^2 \\]
ou encore
\\( a^2+b^2=c^2 \\)

\\\[ a^2+b^2=c^2 \\\]

\[ a^2+b^2=c^2 \]


#### Modéliser un objet en élévation

Il peut s'agir d'un bâtiment inaccessible à hauteur d'homme par exemple. Les résultats attendus seront un modèle 3D texturé, des orthophotographies des façades, etc.

On peut regarder:

* les conseils de [Paladrone en vidéo](https://youtu.be/4Jr9dH7Pasg) pour un acquisition manuelle: **translation → recadrage** → (+ vérification de la distance à l'objet régulièrement)

* Un tuto sur Comment capturer des façades et des constructions sur [becad.net](https://www.becad.net/index.php/contextcapture-guide-dacquisition/#CommentCapturerDesFacadesEtDesConstructions)



#### Modéliser de terrain

Il peut s'agir, sur une grande surface, de faire :	
* de la planimétrie (orthophoto d'une fouille par exemple)

* de la modélisation 3D (de la topographie et/ou des structures en creux/élévation)

* du calcul de cubature (volume des tranchées , décapages et/ou sondages profonds)

  

>**A retenir:**
>
>Longueur photo au sol (en m) = **$m$ = résolution attendue (en cm) x 40**
>
>Largeur photo au sol (en m) = **$n$ = résolution attendue (en cm) x 30**
>
>Hauteur de vol (en m) = **$H$ = résolution attendue (en cm)  x 29,16**
>
>Résolution (en cm/px) = **$R$ = hauteur du drone (en m) x 0,034**
>
>Intervalle de prise de vue longitudinal avec  un chevauchement de 80% (en m)) = **$m$ x 20%**
>
>Intervalle entre 2 lignes de vols avec un chevauchement de 60% (en m)) = **$n$ x 40%**


## I - Caractéristiques du capteur du ![mini2](images/mini2.png)

| **Capteur ** | **DJI Mini 2** |
| -------------------- | ------------ |
| Modèle|Sony IMX  (probablement [IMX377](https://www.sony-semicon.co.jp/e/products/IS/camera/product.html))|
| Taille capteur  (Appellation) | CMOS 1/2,3'' |
| Hauteur (mm) | **4,62**|
|**[Ls]** Largeur (mm) |**6,16** |
|Diagonale(mm) |**7,7**|
|Superficie  (mm²) |28,5|
|Rapport d'image | 1,33 (4/3)|
|Coef. de conversion focale | 5,6 |
| **[f]** Distance focale (mm) | **4,49** |
|**[L x l ]** Taille des photos | 4000 x 3000 Px (12 Mpx)|
| Taille du pixel|		1.55 μm (= 0.0015 mm)|
|FOV| 83°|
|focale (éq. 24x36)|24 mm|
|Ouverture|	f/2,8|
|ISO|100 - 3200|
|Angle de vue|orientable -90° / +20°|



## II - Préparation du vol



### A - Avec un logiciel de *mission planning*

Si le drone n'inclut pas en natif la possibilité de plans de missions, ce qui est le cas du DJI mini 2 et de son apllication DJI Fly, il faut utiliser des applications tierces:

* [ ![logo_pix4d](images/logo_pix4d.png)pix4D capture](https://www.pix4d.com/product/pix4dcapture) gratuit
* [ ![logo_dh](images/logo_dh.png)drone harmony](https://droneharmony.com/) une version loisir gratuiteD
* [ ![logo_dronelink](images/logo_dronelink.png)dronelink](https://www.dronelink.com/) de 20\$ à 80\$

on peut regarder le comparatif de [ 6 planificateurs de missions pour drones par Paladrone](https://youtu.be/9p4s4_JhWVc)

>:warning: Pour pouvoir utiliser ces logiciels tierces il faut :
>
>* le SDK du drone (une API qui permet de communiquer avec le drone sans utiliser l'application native)  et donc que celui-ci soit sorti ([ce qui n'est pas le cas en novembre 2020](https://forum.dji.com/thread-229736-1-1.html))
>* que les logiciels tiers intègrent le SDK



### B - Planification manuelle

Si ni l'application dédiée, ni de logiciels tiers ne peuvent aider à la planification de mission, il faudra  faire la préparation de la mission et l'acquisition des données (pilotage et déclenchement des prises de vues) manuellement.

**Considérons les valeurs suivantes :**

**sources: ** pour les dimensions d'un capteur CMOS 1/2.3'': [luzphoto.com](http://www.luzphotos.com/materiel/apn/taille-capteur-apn-comparatif), pour les caractéristiques du capteur du mini 2: [vidéo de Paladrone](https://www.youtube.com/watch?v=SGESExKLHkU&t=1200s)

|abrev.| description|unité| DJI mini 2|
|:-:|:-|:-:|:-:|
| **Ls **| plus grande dimension du capteur| mm|**6.16**|
|  **ls **| plus petite dimension du capteur| mm |**4.62**|
|**L **| plus grande dimension de la photo |Px|**4000**|
| **l **| plus petite dimension de la photo |Px|**3000**|
| **f **| distance focale de la caméra|mm|**4.49**|
|  **D **| distance entre la caméra et l’objet () | m|$D = f*\frac{m}{Ls}$|
| **H **| hauteur de vol ($= D+f$ note: $f$ étant minime on estime que $H = D$) |m|$H = D$|
| **m **| longueur du plus grand côté du rectangle couvert par une photo au sol |m|$m = R * L$|
|**n **| longueur du plus petit côté du rectangle couvert par une photo au sol |m|$n = R * l$|
|**R **| résolution spatiale au sol des photos |m/Px|$R = \frac{Ls*D}{f*L}$|
|**P **| précision de positionnement dans l’espace des sommets du maillage 3D ||$P = 3 * R$|

  

![https://www.becad.net/index.php/contextcapture-guide-dacquisition/#EstimerLaHauteurDeVol](images/photo_nadir.jpg)

Étant donné que **Ls**, **f** et **L** sont généralement fixes pour un appareil donné, la seule façon d’améliorer  la précision est de diminuer la hauteur de vol. Cela permettra également de conduire à prendre plus de photos pour couvrir la même zone.

**P** est la précision relative du mesh 3D. La précision absolue de votre modèle n’aura un sens que s’il est géoréférencé.

> le modèle peut être géo-référencés, soit en utilisant les positions 3D  importées des photos (grâce à des balises EXIF ou à l’aide d’un fichier  d’importation), ou en ajoutant des points de contrôle (la plupart des  systèmes de référence spatiale sont pris en charge).



####  1) Estimer la hauteur de vol

La hauteur ($H$) de vol dépendra de la  résolution souhaitée pour le modèle.  Le calcul sera basé sur la caméra placée en orientation nadirale.

>  pour l'exemple, on veut obtenir une résolution d' 1cm/pixel

**a) Calcul des dimensions ($m$ et $n$) du rectangle au sol à couvrir pour avoir une résolution de 1 cm/pixel (1 cm = 0.01m):**

Longueur:						Largeur:

$m = R * L $				 	 $n = R * l$

$ m = 0.01 * 4000$			$n = 0.01 * 3000$

$m = 40m$						$n = 30 m$

Chaque photo devra couvrir une surface de 1200 m²

**b) Calcul de la hauteur de vol ($H$) du drone pour obtenir une résolution de 1cm/pixel:**

Hauteur=Distance:

$H = D = \frac{f * R * L}{Ls}$

$ H = f*\frac{m}{Ls}$

$H = 4.49 * \frac{40}{6.16}$

$H = 29.15m$

**→ Le DJI mini 2 devra donc voler à environ 29 mètres du sol pour obtenir une résolution d'1 cm/pixel.**



**Tableau de synthèse:**

| R (en cm/Px) | H (en m) $= R*29.16$ |
| :----------- | -------------------- |
| 0,5          | 14,6                 |
| 1            | 29,2                 |
| 2            | 58,3                 |
| 5            | 145,8                |
| 10           | 291,6                |

:warning: pour préserver la même netteté des  images (éviter les images floues), la vitesse de vol doit être réduite à mesure que l’altitude diminue.



#### 2) Estimer la résolution selon une hauteur de vol	

Les relevés topographiques incluent toujours la précision des résultats. La résolution ($R$) obtenue sera fonction de la hauteur de vol (= distance entre le drone et l'objet).

La formule suivante vous aidera à estimer la précision attendue:

$R = \frac{Ls*D}{f*L}$

> Pour l'exemple le drone sera à 50 m  de hauteur au dessus du sol (avec 50 m = 5000 cm; 6.16 mm = 0.616 cm & 4.49 mm = 0.449 cm)

$R = \frac{0.616*5000}{0.449*4000}$

$R = 1.71 cm/pixel$

**→ Si le  DJI mini 2 vole à  50 mètres du sol on peut espérer obtenir une résolution d'1,7 cm/pixel.**



**Tableau de synthèse:**

| H (en m) | R (en cm/Px) $=H*0.034$ |
| -------- | ----------------------- |
| 1        | 0,03                    |
| 5        | 0,17                    |
| 15       | 0,51                    |
| **30**   | **1,03**                |
| 50       | 1,71                    |
| **60**   | **2,06**                |
| 120      | 4,12                    |



#### 3) Déterminer le plan de vol et la fréquence de prise de vue

Pour la production d'orthophotos,  il est recommandé de réaliser **un chevauchement de 80%  dans le sens d’avancement et de 60% de chevauchement transversalement à  la ligne de vol** pour une acquisition optimale.

![https://lbprofor.com/photogrammetrie-pour-les-drones/](images/chevauchement.jpg)

**a) Distance entre deux photos consécutives dans la même ligne de vol** ($\Delta p$) est déterminée comme ceci :

> CL = Chevauchement Longitudinal = 80%
>
> n = longueur du plus petit côté du rectangle couvert par une photo au sol = 30 m (pour une résolution)

$\Delta p = n * \frac{100-CL}{100} = = n * \frac{100-80}{100} = n * 20\%$ 

$\Delta p = 30 * \frac{20}{100}$

$\Delta p = 6$

→ **Pour obtenir  un chevauchement longitudinal de 80%** (avec le drone à 29m de hauteur, résolution attendue = 1cm/px), **la distance entre 2 prises de vue sur une même ligne est de 6 m**.

**b) Distance entre deux lignes de vol ($\Delta l$)** est déterminée comme ceci :

>  cl = Chevauchement Transversal = 60%
>
> n = longueur du plus petit côté du rectangle couvert par une photo au sol = 30 m (pour une résolution)

$\Delta l = m * \frac{100-cl}{100} = = m * \frac{100-60}{100} = m * 40\%$ 

$\Delta l = 40 * \frac{40}{100}$

$\Delta l = 8$

→ **Pour obtenir  un chevauchement transversal  de 60%** (avec le drone à 29m de hauteur, résolution attendue = 1cm/px), **la distance entre 2 prises de vue est de 8 m**.



**Note:** Méthode de calcul à vérifier:

Pour une surface carrée de 10000m² (SL=100m x sl=100m), il faudra donc

$Nphoto = (\frac{SL}{\Delta p}+1)*(\frac{sl}{\Delta l}+1)$

$Nphoto = (\frac{100}{6}+1)*(\frac{100}{8}+1)$

$Nphoto = (16.66+1)*(12.5+1)$

> Nphoto doit être une entier , on arrondi donc au nombre de photo supérieur:

$Nphoto = 18*14 = 252$

→ **Pour couvrir une surface carrée de 10000m²** (avec une résolution attendue de R=1 cm/px et donc H=29m, CL=80%, cl=60%) **il faudra donc environ 252 prises de vues.**



## C - Acquisition des données: prises de vues



### 1 - Calibration

> Il est à noter que les paramètres de votre appareil photo sont susceptibles de changer au fil du temps (en particulier pour les appareils non métriques). Par conséquent, il est recommandé de calibrer votre appareil photo aussi souvent que possible, et de préférence, juste avant le vol.

Le cheminement pour la mise en œuvre de cette solution est le suivant :
* **Avant le vol, réalisez environ 20 à 30 photos** autour d’un **objet très texturé et géométriquement complexe** avec **les mêmes propriétés de la caméra que celles qui seront utilisées durant le vol** ;
* Réalisez votre acquisition aérienne ;
* Effectuez l’aérotriangulation des 20 à 30 photos de votre objet (configuration non critique) et ajouter le calibrage résultant de l’appareil à la base de données comme expliqué précédemment ;

**source:** [becad.net](https://www.becad.net/index.php/contextcapture-guide-dacquisition/#ConfigurationDAcquisitionPourUASUAVEtDrones)



## D - Traitement

### 1 - Agisoft Metashape

cf. pas à pas - Metashape de la formation Inrap

### 2 - Mic-Mac



## E - Ressources

### vidéos

* Tests et conseils d'acquisition manuelle avec un DJI mini 2: [Paldrone - Photogrammétrie la captation en manuel - DJI Mini 2 - RealityCapture](https://youtu.be/4Jr9dH7Pasg)
* Série de 2 vidéos sur les concepts et calculs de résolution:
  *  part 1 - [Résolution et drone - le concept](https://youtu.be/IuPZsMGbEUc) 
  * part 2 - [Résolution et drone - méthode de calcul](https://youtu.be/yaLqGvUYzzg)

### tutos

article sur l'acquisition des données par drone: calibration, calculs, etc [becad.net](https://www.becad.net/index.php/contextcapture-guide-dacquisition/#ConfigurationDAcquisitionPourUASUAVEtDrones). Note: article dédié à l'origine  aux utilisateurs du logiciel ContextCapture.

### logiciels

* ![metashape](images/logo_metashape.png) **Agisoft Metashape** (*cracké* :skull_and_crossbones: )
  * trouvé via youtube [Geo World - How to install Agisoft Metashape with crack |Easy method](https://youtu.be/fAPH9K12tPU) consulté le 7/11/21
  * fichiers sur [Gdrive](https://www.youtube.com/redirect?q=https%3A%2F%2Fdrive.google.com%2Fopen%3Fid%3D1wOmC5WeN5pJ_pxHs5c7SfcRkTwTT7KPI&event=video_description&redir_token=QUFFLUhqa3Z4a1Y4YWpiRkZqLVk0WGc5blg0VWpEVEwtQXxBQ3Jtc0tuelVrdzliM0M0LURqWS1DQ0lxUEFmbE1tbFFCOWx6Rm1MZnJFUXpqdy1OTlRvWklWSzdIVW9WWjVvUktiQUcxMGN6WWxTTXEwalFJR3Q2WDVpcXhvTzJBbTViVnZMTDRPc1YtOG1feEdlbWdyY3Z4cw%3D%3D&v=fAPH9K12tPU)