# Photo

Rappels et conseils sur la photographie et la prise de vue

## capteur 

| Appareil     | Taille de capteur ( en mm) |diagonale (mm) | Taille de capteur (en pouces) | coefficient de conversion de focale |
| ------------ | ---------------------- | -------------------------- | ------------------------- | ------------------------- |
| plein format | 24 x 36                | 43,3 |                          | 1                         |
| **Nikon D90** |15,6 x 23,5 |28,2||1,5|
|**Osmo Action** |4,62 x 6,16|7,7|1/2,3|5,6|
|**mini 2** |4,62 x 6,16|7,7|1/2,3|5,6|

> ex: Avec le Nikon D90, un objectif 50 mm (FOV -Field Of View 46°) cadre en fait comme un 75 mm plein format
>
> ex: L'Osmo Action comme le mini 2 ont un capteur Sony IMX377 de 1/2,3":
>
> * L'Osmo Action avec une focale de X,XX mm / FOV  89°-145° équivalent à a une focale de 6,82 mm-21 mm en plein format
> * Le mini 2 a une focale de 4,49 mm / FOV 83° équivalent  a une focale de 24/25 mm en plein format







## vitesse

Sans trépied:

* Avec un plein format, prendre la focale et **ne pas descendre en dessous de 1/focale**

:warning: **Il faut appliquer le coefficient multiplicateur !** → ![$1/(f * cf)$](images/math_1.png)



> Théoriquement donc, avec un objectif de 50mm (sans stabilisateur):
>
> * Avec un Plein Format ne pas descendre en dessous de 1/50 s (1/100 s = :ok: , 1/10 s = :no_entry: )
> * Avec un Nikon D90 ne pas descendre en dessous de 1/75 s
>
> Avec le mini 2 et son équivalent 24 mm il ne faudrait pas descendre en dessous de 1/25
>
> 

* :warning: Il faut appliquer le coefficient multiplicateur ! → ![$1/(f * cf)$](images/math_1.png) *ex: Sur un Nikon D90 par exemple, à 200 sur un 7D on a donc une durée d'obturation de l'ordre de 1/320s*

Pour un sujet fixe avec une grand angle, ne pas descendre en dessous de  1/60s, en fait la règle au delà c'est d'être supérieur à la focale (ex:  avec une focale 400mm, être au moins à 1/500s...etc...). Ensuite tu as  les boitiers et objectif avec stab qui permet de descendre un peu en  vitesse.
 Ensuite pour palier à ta courte vitesse et éviter le trépied, tu peux  augmenter les ISO, mais là il y a une réel limite, pour le mien je ne  monte pas au delà de 800 iso. 



# Vidéo

Rappels et conseils sur la vidéo



## FPS & vitesse

La loi des 180°: pour un effet "cinéma" régler la le dénominateur de la vitesse au double de celui du nombre d'image par secondes (FPS)

> exemple: si on filme en 30 fps il faut régler la vitesse de l'obturateur à 1/60"