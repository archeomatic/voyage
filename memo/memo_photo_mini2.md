# Drone et photographie

Notes sur la photographie par drone notamment les post traitement possibles


## Tests du capteur

### résolution

|Hauteur de vol|Résolution (en cm/pixel)|Image (50x50cm)|
|---|---|---|
|5|0.15|![5m.JPG](images/5m.JPG)|
|10|0.32|![10m.JPG](images/10m.JPG)|
|20|0.64|![20m.JPG](images/20m.JPG)|
|30|0.93|![30m.JPG](images/30m.JPG)|
|40|1.20|![40m.JPG](images/40m.JPG)|
|50|1.51|![50m.JPG](images/50m.JPG)|



--------------------



## Post-traitements



### Photo *tiny world*

* ![mini2_67.png](images/mini2_67.png) Le drone DJI mini 2 dispose d'un **mode panoramique → sphere** :
  * Récupérer la photo panoramique en la téléchargeant via le smartphone

![1_pano360.png](images/1_pano360.png)

{% hint style='zob' %}
La photo panoramique assemblée n'est pas sur la carte SD, il faut impérativement la télecharger ou l'envoyer par mail depuis le smartphone connecté au drone.
{% endhint %}  



* ![logo_gimp](images/logo_gimp.png) Avec **Gimp**:

  * Modifier la taille de l'image pour quelle soit carrée avec le nombre maximum de pixels de coté:

  > Note: L'image fait 4096x2048px au départ et 4096x4096px à l'arrivée

  **Image → Échelle et taille de l'image →** ![2_pano360_taille.png](images/2_pano360_taille.png)

  ![3_pano360_caré.png](images/3_pano360_caré.png)

  * Retourner l'image verticalement:

  **Image → Transformer → Miroir vertical**

  * Appliquer une distorsion des coordonnées polaires

  **Filtres → Distorsions → Coordonnées polaires..**

  ![4_pano360_tiny.png](images/4_pano360_tiny.png)

  * Pour finaliser, on peut:

    * refaire un **miroir vertical** pour mettre le sujet principal en haut
    * utiliser la **baguette magique** pour supprimer le contour noir
    * ne garder que le ciel nuageux (Outils→Outils de sélection→**sélection elliptique**)
  
  ![5_pano360_tiny.png](images/5_pano360_tiny.png)


### Panorama 360°

{% hint style='working' %}
Un exemple hébergé sur gitlab →  https://archeomatic.gitlab.io/voyage/pano360
{% endhint %} 

* ![mini2_67.png](images/mini2_67.png) Le drone DJI mini 2 dispose d'un **mode panoramique → sphere** :

  * Récupérer la photo panoramique en la téléchargeant via le smartphone

  

*  ![logo_marzipano_67.png](images/logo_marzipano_67.png) Depuis le site [www.Marzipano.net](www.Marzipano.net), utiliser l'outil **[Marzipano Tool]** en cliquant sur le bouton idoine.

  * Charger la photo panoramique → paramétrer → Exporter le fichier zip

  {% hint style='danger' %}
  Il n'y a pas de système de sauvegarde du projet en ligne on peut seulement l'exporter. On ne pourra donc pas le modifier en ligne par la suite...
  {% endhint %} 

  * Transférer le contenu du dossier **app-files** sur un serveur web (avec **Filezilla** par ex.) ou simplement dans un dossier **gitlab**

    ![pano360_vv_files](images/pano360_vv_files.png)

* Pour aller plus loin il faut éditer les fichiers javascript, html et css

{% hint style='zob' %}
On peut s'aider des exemples fournis avec le code source sur la [page Demos](https://www.marzipano.net/demos.html) (nombreux exemples avec prise en charge VR, zoom et autres) et du tuto de [Dzone](https://dzone.com/articles/build-virtual-tours-of-houses-and-parks-with-open) (notamment pour l'hébergement sur github)
{% endhint %} 

{% hint style='info' %}

* Pour tester d'autres solutions libres, consulter la page référençant [les meilleures solutions opensource de visite virtuelle](https://www.goodfirms.co/blog/best-free-open-source-virtual-tour-software-solutions)
* On peut utiliser aussi une solution avec hébergement comme [klapty.com](https://tour.klapty.com/wzZ0RztAPD/) qui impose *seulement* son logo incrusté et quelques limitations.
* D'autres solutions ont été essayées comme [tourmake](https://myarea.tourmake.it/fr/areafoto/viewmake/ante/72397) (pas compris le système du "gratuit mais avec renouvellement") et [kuula](https://kuula.co/share/collection/7PQPC?fs=1&vr=0&sd=1&thumbs=1&info=1&logo=1) (beaucoup de réglages pour améliorer l'image et paramétrer la navigation (ex: angle max haut-bas)
version gratuite sans les *hotspots*, ni points d'info)
{% endhint %} 

### Cartographier les photos

* le drone DJI mini 2 enregistre sa position GPS lors d'une prise de vue dans les métadonnées EXIF
* Pour consulter simplement les données EXIF il existe par exemple le logiciel gratuit  ![logo_xn.png](images/logo_xn.png)**XNview**  . Il suffit de cliquer sur l'icône info et choisr l'onglet EXIF:
![xnview](images/xnview.png)



* On utilise  ![logo_qgis.png](images/logo_qgis.png)**QGIS** et l'outil **Import de photos géolocalisées** pour cartographier automatiquement un dossier de photos du drone

  * Dans QGIS, Menu <u>T</u>raitement → Boîte à outils → Création de vecteurs → Import de Photos géolocalisées
  ![traitement](images/traitement.png)
  * Choisir un dossier photo et [Executer]
    ![traitement2](images/traitement2.png)
  ​	→  QGIS a créé une couche de points contenant l'emplacement du drone au moment  de la prise de vue (du drone, pas du sujet malheureusement ;) avec une  table attributaire contenant: **nom de la photo**, **répertoire de la photo** (la photo s'affiche automatiquement en mode formulaire cf.imprim'ecran, **altitude**, **direction** (nope? dommage), **longitude**, **latitude**, **timestamp** (date et heure à la seconde près)

![qgis_photo_geoloc.png](images/qgis_photo_geoloc.png)



### Récupérer et cartographier les données de vol

* Le drone DJI mini 2 enregistre des données de vol via l'application DJI Fly.

  

* Récupérer les fichiers contenant les données de vol:

  *  Sur le smartphone Android ces fichiers **FligthRecord** se situent dans le dossier **Mes Fichiers → Stockage Interne → DJI → dji.go.v5 → FlightRecord**
  * Sélectionner les fichiers du type *DJIFlightRecord_2021-01-10-[16-32-42].txt* et les envoyer par mail.

* Se rendre sur le site [PhantomHelp.com](https://www.phantomhelp.com/LogViewer/Upload/) :

  * Charger le fichier *DJIFlightRecord_2021-01-10-[16-32-42].txt*

  <img src="images/phantomhelp1.png" alt="phantomhelp" style="zoom:80%;" />
  
  * Une fois le fichier chargé, le site vous propose:
  
    * De télecharger le fichier au format **kml** (kmz en fait) et **csv**
    * De consulter la progression du vol
    * De consulter toutes les données de vols sous la forme d'un tableau

![phantomhelp2](images/phantomhelp2.png) 
    

> ![logo_qgis.png](images/logo_qgis.png) Dans QGIS le fichier csv est facilement importable avec la fonction ![logo_import_csv](images/logo_import_csv.png) **Ajouter une couche de texte délimité**
>
> ![qgis_import_csv](images/qgis_import_csv.png)
>
> * Parcourir et charger le fichier
> * Vérifier la **géométrie: point** et définir le **X (Longitude)** et le **Y (Latitude)**, et éventuellement le Z (Altitude (mètres))
> *  Choisir le Système de Coordonnées de Réference (SCR) **EPSG:4326 - WGS84**
> *  Cliquer sur **[Ajouter]**
>



![qgis_flight_record](images/qgis_flight_record.png)



{% hint style='zob' %}
Les informations Pitch, Roll et Yaw (et Yaw 360) concernent la nacelle de l'appareil photo. Ce sont des paramètres que l'on utilise pour définir un espace 3D.

Par exemple pour les casques de réalité virtuelle:

<img src="images/yaw_pitch_roll.jpg" alt="yaw_pitch_roll.jpg" style="zoom:67%;" />

Note: c'est ce que mesure l'IMU (Unité de Mouvement Inertiel) du drone

{% endhint %}  

