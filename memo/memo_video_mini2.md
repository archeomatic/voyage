# Drone et vidéo

Notes sur la vidéo par drone



## FPS & vitesse

La loi des 180°: pour un effet "cinéma" régler la le dénominateur de la vitesse au double de celui du nombre d'image par secondes (FPS)

> exemple: si on filme en 30 fps il faut régler la vitesse de l'obturateur à 1/60"



### transitions

* voir le guide DJI sur [le top 10 des transitions](https://store.dji.com/guides/top-10-drone-transition-shots-for-aerial-footage/)
