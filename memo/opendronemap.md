# Cartographier avec un drone et OpenDroneMap / WebODM



![odm_logo_small](images/odm_logo_small.png)[**OpenDroneMap**](https://www.opendronemap.org/) est une boîte à outils en ligne de commandes  pour faire de la photogrammétrie et de la cartographie à l'aide d'un drone.

![odm_webodm_logo_small](images/odm_webodm_logo_small.png)[**WebODM**](https://www.opendronemap.org/webodm/) est une interface pour OpenDroneMap. 

{% hint style='info' %}

* :dollar: Il existe une version gratuite qui nécessite une installation à la  :hand: et différentes versions payantes (57$ à vie).
* :book: La documentation, en partie traduite en français, est complète, relativement abordable et disponible sur [https://docs.opendronemap.org/fr/](https://docs.opendronemap.org/fr/)

{% endhint %}


-----------------------------
# 1. Installation(s)

> :information_source: testé sur un portable DELL Précision 3561 - 16Go RAM - SSD 1To avec Windows 10 Professionnel le 10/04/2023

* Pour un premier essai je choisi de ne pas télécharger l'installeur (même s'il ne coûte que 57 :dollar:  à vie ! )

* Les instructions d'installation en français sont disponible sur [la documentation en ligne d'ODM](https://docs.opendronemap.org/fr/installation/#installation)

* Cela peut être un peu galère :rowboat: mais ce n'est théoriquement à ne faire qu' :one: fois par ordinateur.

{% hint style='danger' %}
La configuration minimale requise pour exécuter le logiciel est la suivante:

- Processeurs 64 bits fabriqués à partir de 2010
- espace de 20 Go sur le disque
- 4 GB de RAM
  {% endhint %}

## Étape 1:  l'ordinateur accepte-t'il la virtualisation ?

*Checker* la capacité de l'ordinateur *-windows 10 professionnel en l’occurrence-* a accepter la virtualisation:

Pour vérifier que la virtualisation est acceptée comme indiquée dans la [documentation](https://docs.opendronemap.org/fr/installation/#step-1-check-virtualization-support): appuyez sur [CTRL]+[Maj]+[ESC] et passer à l’onglet [**Performance**] → vérifier que la virtualisation est activée.

<img src="images/odm_virtu_activé.png" alt="odm_virtu_activé.png" style="zoom:50%;" />



## Étape 2: Installer les logiciels pré-requis (Python 3, Git for Windows et Docker Desktop)

* :snake: Installer [Python 3](https://www.python.org/downloads/) (:warning:Cocher l'option *Add Python 3.7 to path* quand elle est proposée)

  >  Si Python est déjà installé: Pour vérifier la version de Python installée avec une fenêtre de commande ([bouton windows] puis `cmd` → taper `Python --version`)

* ![icon_git.png](images/icon_git.png)Installer [git for Windows](https://git-scm.com/download/win)

* :whale: Installer [Docker Desktop](https://www.docker.com/products/docker-desktop/)

  

 * Exécuter :whale: **Docker Desktop**

   > :warning: Si message d'erreur pour la mise à jour du WSL → suivre le [lien microsoft](https://learn.microsoft.com/fr-fr/windows/wsl/install-manual#step-4---download-the-linux-kernel-update-package) et télecharger le [Package de mise à jour du noyau Linux WSL2 pour machines x64](https://wslstorestorage.blob.core.windows.net/wslblob/wsl_update_x64.msi) puis Exécuter le fichier **wsl_update_x64.msi** 



## Étape 3: Cloner et installer WebODM

Une fois tout installé et paramétré il ne reste plus qu'à cloner **WebODM** depuis **GitHub** dans un répertoire local (...sur l'ordi quoi !) et l'installer.

* Dans le répertoire voulu (`C:\` par exemple), avec Git : *clic-droit* → ![icon_git.png](images/icon_git.png)*Git Bash Here*

  

  * Cloner le répertoire WebODM depuis GitHub

    > Taper (ou faire un copier → dans Git: *clic-droit* → *Paste*) dans la fenêtre de commande:
    >
    > `git clone https://github.com/OpenDroneMap/WebODM --config core.autocrlf=input --depth 1`

  → Un répertoire :file_folder: WebODM contenant tous les fichiers requis a été créé

  ​	

  * Accéder au répertoire WebODM:

    > Taper `cd WebODM`

  * Exécuter WebODM

    > Taper `./webodm.sh start`

![webodm_git_install](images/webodm_git_install.png)

... :clock10: attendre patiemment jusqu'à l'installation complète :clock1130:...

![webodm_git_install_end](images/webodm_git_install_end.png)



## Étape 4: Exécuter WebODM pour la 1ère fois

* Dans un explorateur internet accéder à l'adresse `http://localhost:8000/`

* A la première connexion il faut créer son compte

<img src="images/webodm_creation_compte.png" alt="creation de compte WebODM" style="zoom:50%;" />



# 2. Lancement de WebODM



{% hint style='working' %}

Il faut d'abord avoir réussi :champagne:  à installer toute la machinerie (cf. [1. Installation(s))](#3.Installation(s))

{% endhint %}



* Il suffit juste d'exécuter :whale: **Docker Desktop** et vérifier que webodm est en cours d'éxecution.

![webodm_docker](images/webodm_docker.png)

* Ensuite d'accéder à WebODM avec un explorateur internet via l'adresse http://localhost:8000/

  

{% hint style='working' %}

Pour interragir avec WebODM, il suffit, depuis le dossier [WebODM], d'ouvrir une nouvelle fenêtre de Terminal **Git** (clic-droit → *Git Bash Here*) et de taper des commandes. Par <u>exemple</u>:

Par exemple, pour lancer **WebODM** avec **Mic Mac**:

* Il suffit de démarrer WebODM avec la commande `./webodm.sh restart --with-micmac`

Pour **arrêter proprement WebODM**:

* Taper la commande `./webodm.sh stop`

Pour tout mettre à jour:

* Taper la commande `./webodm.sh update`

{% endhint %}


------------------------
# 2. Utilisation



## Utiliser des GCP

les GCP (Ground Point Control) sont des points dont on connait précisémment la position en X,Y et Z au sol.

{% hint style='working' %}

A faire avant de charger les photos dans le projet !

{% endhint %}

Créer un fichier **prepa_gcp.txt** de type:

projection au format proj4
[nom du gcp]	[geom_x]	[geom_y] 	[geom_z]

par exemple:

+proj=lcc +lat_0=47 +lon_0=3 +lat_1=46.25 +lat_2=47.75 +x_0=1700000 +y_0=6200000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs
A	1532570.3629	6234410.2818	84.1946
B	1532570.2085	6234409.6535	84.1874
C	1532569.9489	6234409.1122	84.1837

Dans l'interface GCP de WebODM

Importer (par glisser-déposer par ex) le fichier **prepa_gcp.txt** ainsi que toutes les photos susceptibles de contenir un ou des gcp.

Cliquer sur le gcp sur la carte (panneau à droite) puis en haut du panneau image (à gauche) sur le + puis à l'endroit du gcp sur la photo. les 2 cibles sur l'image et sur le plan doivent être vertes et réagissent au survol de la souris.

Une fois le travail terminé.... OUF

Exporter le fichier et l’appeler IMPERATIVEMENT  **gpc_list.txt** et l**'enregistrer dans le même dossier que les photos**.

Si vous ouvrez le fichier gcp_list.txt avec le bloc note, il devrait ressembler à ceci:

+proj=lcc +lat_0=47 +lon_0=3 +lat_1=46.25 +lat_2=47.75 +x_0=1700000 +y_0=6200000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs	
1532568.41	6234410.29	84.1923	413.11	1539.56	IMG_1575.JPG	F
1532568.41	6234410.29	84.1923	2552.38	1774.60	IMG_1591.JPG	F
1532568.41	6234410.29	84.1923	1425.00	1917.46	IMG_1591.JPG	F
1532568.41	6234410.29	84.1923	2418.76	962.73	IMG_1614.JPG	F
1532569.50	6234410.66	84.1958	739.95	409.96	IMG_1575.JPG	H
1532569.50	6234410.66	84.1958	753.49	349.19	IMG_1576.JPG	H

→ vous pouvez maintenant charger les photos et le ficher gcp_list.txt dans le projet !



## Faire des images- masques

Des images masques permettent de dire au logiciel d'ignorer une partie de la photo dans les calculs. Ceci est quasi-indispensable si sur les photos apparaissent des surfaces miroir (eau, vitre,....), du ciel, ou des surfaces complexes (grilles) ou inutiles.

Avec ![gimp](images/logo_gimp.png)**Gimp**:

* ouvrir la photo

* Dans le panneau calque: clic droit → ajouter un canal alpha

* Ajouter un calque à fond blanc et le mettre en dessous

* Sur l'image avec le pinceau [P] (ou un autre outil)  dessiner en noir la partie à masquer

* avec la baguette magique [U] sélectionner le noir que vous avez dessiné (avec la touche [Ctrl] enfoncé si plusieurs parties) → Inverser la sélection [Ctrl]+[I] → supprimer [suppr]

  > Vous devriez avoir une image à fond blanc avec un un (ou des) pâté(s) noir ! :clap:

* Exporter [ctrl]+[Maj]+[E] sous le même format que l'image d'origine en ajoutant **_mask** au nom d'origine

  > IMG0026.JPG sera donc accompagné de son masque IMG0026_mask.JPG
  
  

---------------

