# Cartographier avec un drone et OpenDroneMap / WebODM



![odm_logo_small](images/odm_logo_small.png)[**OpenDroneMap**](https://www.opendronemap.org/) est une boîte à outils en ligne de commandes  pour faire de la photogrammétrie et de la cartographie à l'aide d'un drone.

![odm_webodm_logo_small](images/odm_webodm_logo_small.png)[**WebODM**](https://www.opendronemap.org/webodm/) est une interface pour OpenDroneMap. 

{% hint style='info' %}

* :dollar: Il existe une version gratuite qui nécessite une installation à la  :hand: et différentes versions payantes (57$ à vie).
* :book: La documentation, en partie traduite en français, est complète, relativement abordable et disponible sur [https://docs.opendronemap.org/fr/](https://docs.opendronemap.org/fr/)

{% endhint %}


-----------------------------
# 1. Lancement de WebODM



{% hint style='working' %}

Il faut d'abord avoir réussi :champagne:  à installer toute la machinerie (cf. [2. Installation)](#2.Installation(s))

{% endhint %}



* Pour commencer, redémarrer l'ordinateur et ne pas ouvrir d'explorateur internet :interrobang: *(à vérifier)*

* Avec l'explorateur de fichiers, se rendre **dans le dossier [WebODM]**

  >  C'est le dossier qui a été cloné depuis GitHub lors de l’installation, *C:/git/WebODM pour moi*

* Lancer le terminal **Git**: clic droit → *Git bash here* 

* Dans la fenêtre du terminal,  **exécuter ces 3 commandes à la suite, en attendant à chaque fois la fin du processus**, c'est à dire l’affichage d'une nouvelle ligne avec un `$`.

  ![odm_launch](images/odm_launch.png)

  * `docker-machine start default` → permet de lancer la VM de docker

  * `docker-machine ip` → récupérer l'adresse IP de docker (de type 198.168.99.100)

  * `./webodm.sh restart` pour (re-)démarrer WebODM

    → attention ce dernier processus peut être long.... attendre le message indiquant d'ouvrir un navigateur internet et de taper l'adresse `http://localhost:8000`

    ![odm_launch2](images/odm_launch2.png)

    

* Ouvrir un **explorateur internet** et **taper dans la barre d'adresse  `http://192.168.99.100:8000`** en remplaçant `192.168.99.100`par l'adresse IP renvoyée précédemment par la commande `docker-machine ip`

  * renter votre :man: login et :lock: mdp (ou si c'est la première fois créez le)
  * voili ! :helicopter:  Opendronez bien !

  ![odm_chrome](images/odm_chrome.png)



{% hint style='working' %}

Pour interragir avec WebODM, il suffit, depuis le dossier [WebODM], d'ouvrir une nouvelle fenêtre de Terminal **Git** et de taper des commandes. Par exemple:

Pour lancer **WebODM** avec **Mic Mac**:

* Il suffit de démarrer WebODM avec la commande `./webodm.sh restart --with-micmac`

Pour **arrêter proprement WebODM**:

* Taper la commande `./webodm.sh stop`

Pour tout mettre à jour:

* Taper la commande `./webodm.sh update`

{% endhint %}


------------------------
# 2. Utilisation



## Utiliser des GCP

les GCP (Ground Point Control) sont des points dont on connait précisémment la position en X,Y et Z au sol.

{% hint style='working' %}

A faire avant de charger les photos dans le projet !

{% endhint %}

Créer un fichier **prepa_gcp.txt** de type:

projection au format proj4
[nom du gcp]	[geom_x]	[geom_y] 	[geom_z]

par exemple:

+proj=lcc +lat_0=47 +lon_0=3 +lat_1=46.25 +lat_2=47.75 +x_0=1700000 +y_0=6200000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs
A	1532570.3629	6234410.2818	84.1946
B	1532570.2085	6234409.6535	84.1874
C	1532569.9489	6234409.1122	84.1837

Dans l'interface GCP de WebODM

Importer (par glisser-déposer par ex) le fichier **prepa_gcp.txt** ainsi que toutes les photos susceptibles de contenir un ou des gcp.

Cliquer sur le gcp sur la carte (panneau à droite) puis en haut du panneau image (à gauche) sur le + puis à l'endroit du gcp sur la photo. les 2 cibles sur l'image et sur le plan doivent être vertes et réagissent au survol de la souris.

Une fois le travail terminé.... OUF

Exporter le fichier et l’appeler IMPERATIVEMENT  **gpc_list.txt** et l**'enregistrer dans le même dossier que les photos**.

Si vous ouvrez le fichier gcp_list.txt avec le bloc note, il devrait ressembler à ceci:

+proj=lcc +lat_0=47 +lon_0=3 +lat_1=46.25 +lat_2=47.75 +x_0=1700000 +y_0=6200000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs	
1532568.41	6234410.29	84.1923	413.11	1539.56	IMG_1575.JPG	F
1532568.41	6234410.29	84.1923	2552.38	1774.60	IMG_1591.JPG	F
1532568.41	6234410.29	84.1923	1425.00	1917.46	IMG_1591.JPG	F
1532568.41	6234410.29	84.1923	2418.76	962.73	IMG_1614.JPG	F
1532569.50	6234410.66	84.1958	739.95	409.96	IMG_1575.JPG	H
1532569.50	6234410.66	84.1958	753.49	349.19	IMG_1576.JPG	H

→ vous pouvez maintenat charger les photos et le ficher gcp_list.txt dans le projet !



## Faire des images- masques

Des images masques permettent de dire au logiciel d'ignorer une partie de la photo dans les calculs. Ceci est quasi-indispensable si sur les photos apparaissent des surfaces miroir (eau, vitre,....), du ciel, ou des surfaces complexes (grilles) ou inutiles.

Avec ![gimp](images/logo_gimp.png)**Gimp**:

* ouvrir la photo

* Dans le panneau calque: clic droit → ajouter un canal alpha

* Ajouter un calque à fond blanc et le mettre en dessous

* Sur l'image avec le pinceau [P] (ou un autre outil)  dessiner en noir la partie à masquer

* avec la baguette magique [U] sélectionner le noir que vous avez dessiné (avec la touche [Ctrl] enfoncé si plusieurs parties) → Inverser la sélection [Ctrl]+[I] → supprimer [suppr]

  > Vous devriez avoir une image à fond blanc avec un un (ou des) pâté(s) noir ! :clap:

* Exporter [ctrl]+[Maj]+[E] sous le même format que l'image d'origine en ajoutant **_mask** au nom d'origine

  > IMG0026.JPG sera donc accompagné de son masque IMG0026_mask.JPG
  
  

---------------

# 3. Installation(s)



* Pour un premier essai je choisi de ne pas télécharger l'installeur (même s'il ne coûte que 57 :dollar:  à vie ! )

* Les instructions d'installation en français sont disponible sur [la documentation en ligne d'ODM](https://docs.opendronemap.org/fr/installation.html)

* Cela peut être un peu galère :rowboat: mais ce n'est théoriquement à ne faire qu' :one: fois par ordinateur.

{% hint style='danger' %}
La configuration minimale requise pour exécuter le logiciel est la suivante:

- Processeurs 64 bits fabriqués à partir de 2010
- espace de 20 Go sur le disque
- 4 GB de RAM
{% endhint %}

## Étape 1:  l'ordinateur accepte-t'il la virtualisation ?

*Checker* la capacité de l'ordinateur *-windows 7 en l’occurrence-* a accepter la virtualisation:

* télécharger un (tout) petit logiciel **havdetection.exe**  de 179ko, depuis le site de [Microsoft ](https://www.microsoft.com/en-us/download/details.aspx?id=592)

* exécuter le logiciel en double-cliquant et vérifier qu'il affiche le message suivant:
![odm_hav](images/odm_hav.png)

## Étape 2: Docker + Virtual Box

### Installer Python 3.X et docker

* :snake: Vérifier Python 3 + Add Python 3.7 to path

  >  :interrobang: *N’oubliez pas d’ajouter l’exécutable Python à votre PATH (pour que vous puissiez exécuter des commandes avec lui)*

* :whale: Installer **Docker** depuis [github](https://github.com/docker/toolbox/releases/download/v18.09.3/DockerToolbox-18.09.3.exe) (217mo) ( :warning:attention lien différent selon la version de Windows) 

  → En fait il va installer Docker, Virtualbox, Git for Windows,... 

  >  Ne choisir que les logiciels que l'on ne possède pas déjà ou que l'on veut mettre à jour.

  

 * Double-Cliquer sur le raccourci bureau  ![icon_docker](images/icon_docker.png)**Docker QuickStart Terminal**
→ **attendre patiemment** :hourglass_flowing_sand: l'écran de fin:
![odm_docker_install](images/odm_docker_install.png)

  

  ### Configurer la Machine Virtuelle (VM)

Une fois installé **Docker Toolbox** :

1. Ouvrir l’application **Oracle VM VirtualBox**
2. Faites un clic droit sur la VM **default** →  **Fermer → Extinction par ACPI ** pour arrêter la machine
3. Faites un clic droit sur la VM **default ** → **Configuration…**
4. Panneau **Système** → Onglet [Carte-mère] : allouer 60 à 70% de la mémoire en déplaçant le *slider*, (4 Mo minimum)
![odm_vm_config](images/odm_vm_config.png)
  
5. éventuellement, allouer la moitié des Processeurs disponibles (Onglet [Processeur]) (2 CPU minimum)
  ![odm_vm_config2](images/odm_vm_config2.png)
6. Vérifier en bas de la fenêtre, si des paramètres invalides ont été détectés, les corriger le cas échéant → *ici, manque de mémoire vidéo allouée*
  ![odm_vm_config3](images/odm_vm_config3.png)
7. Cliquer sur [OK]



## Étape 3: Cloner WebODM



{% hint style='working' %}

Il faut que **Git** pour Windows ait été installé...

{% endhint %} 

Une fois tout installé et paramétré il ne reste plus qu'à cloner **WebODM** depuis **GitHub** dans un répertoire local (...sur l'ordi quoi !)

* Dans un répertoire dédié , faire un clic droit → *Git bash here* → taper `git clone https://github.com/OpenDroneMap/WebODM`

* Un dossier **WebODM** a été créé dans ce répertoire 

  >  * pour moi le dossier sera *C:\git\WebODM*
  >  * Il est possible de faire un raccourci de ce dossier sur le bureau pour y accéder plus facilement
