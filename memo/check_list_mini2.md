# Check-list mini 2



## Avant de partir

| Titre            | Description                                                  | :ballot_box_with_check: |
| ---------------- | ------------------------------------------------------------ | :---------------------: |
| **Mises à jour** | Vérifier les mises à jour du *firmware* pour le drone et la radio commande |                         |
| **Batteries**    | Vérifier le chargement des batteries à 100%                  |                         |
| **Smartphone**   | Vérifier le chargement du smartphone                         |                         |
| **Carte SD**     | Vérifier que la carte microSD est vide (et formatée)         |                         |
| **Hélices**      | Vérifier l'état des hélices                                  |                         |
| **Météo**        | Vérifier les conditions météorologiques                      |                         |
| **Sac**          | Vérifier le matériel dans le sac d'après la liste **contenu du sac** |                         |

## Contenu du sac

| Objet     | Descriptif                                                   | :notebook: Manuel                                          | :ballot_box_with_check:|
| --------- | ------------------------------------------------------------ | :----------------------------------------------------------- | ------------------------------------------------------------ |
| **Mini 2** | [DJI mini 2](https://www.dji.com/fr/mini-2/specs) | [![mini2](images/mini2.png)](https://dl.djicdn.com/downloads/DJI_Mini_2/20201130/DJI_Mini_2_User_Manual_EN.pdf) |   |
| MicroSD 128Go | [Carte SanDisk Extreme microSD 128 Go U3 V30](https://store.dji.com/fr/product/sandisk-micro-sd-card-128gb) | [![sd128](images/sd128.png)]() |   |
| Radio commande | [DJI Controleur mini 2](https://www.dji.com/fr/mini-2/specs) | [![controleur_mini2](images/controleur_mini2.png)](https://dl.djicdn.com/downloads/DJI_Mini_2/20201130/DJI_Mini_2_User_Manual_EN.pdf) |   |
| Batteries de vol | [DJI Batterie de Vol Intelligente 2250 mAh](https://store.dji.com/fr/product/mini-2-intelligent-flight-battery) | [![batt_mini2](images/batt_mini2.png)]() |      |
| Chargeur 3A | [DJI Chargeur 18W 5V/3A](https://store.dji.com/fr/product/dji-18w-usb-charge) | [![chargeur_mini2](images/chargeur_mini2.png)]() |     |
| Station de recharge | [DJI Hub de charge  USB 18W + powerbank](https://store.dji.com/fr/product/mini-2-two-way-charging-hub) | [![hub_mini2](images/hub_mini2.png)]() |                 |
| Hélices + tournevis | [Hélices de rechanges + tournevis + vis]() | |
| Landing Pad | [PGYTECH - Tapis D’atterrissage Pliable ](https://www.pgytech.com/collections/for-dji-mini-2/products/landing-pad-pro-for-drones) | [![landing_pad](images/landing_pad.png)]()|
|||||
|**Smartphone**|[Samsung - S7 Edge](https://www.samsung.com/fr/support/model/SM-G935FZKAXEF/)|[![smartphone](images/smartphone.png)](https://org.downloadcenter.samsung.com/downloadfile/ContentsFile.aspx?CDSite=UNI_FR&OriginYN=N&ModelType=N&ModelName=SM-G935F&CttFileID=7031446&CDCttType=UM&VPath=UM%2F201805%2F20180504175845092%2FSM-G935_UM_Open_Oreo_Fre_Rev.1.0_180504.pdf)||
| **Batterie Externe** | [VIVIS Power Bank 20000 mAh](https://portablebatteries.fr/tests-avis/vivis-20000mah/) | [![powerbank_20k](images/powerbank_20k.png)]() |   |

##  Préparation du vol
| Titre            | Description                                                  | :ballot_box_with_check: |
| ---------------- | ------------------------------------------------------------ | :---------------------: |
| **No Fly Zone** | Vérifier la carte des NFZ / Géoportail ||
| **Firmware** | Vérifier  -une dernière fois-  les mises à jour du *firmware* pour le drone et la radio commande ||
| **Vent** | Pas trop de vent, surtout en bourrasques (attention: plus de vent à 100 m de hauteur qu’à ras du sol !) **mini 2 = 8 à 10 m/s max** ||
| **Inspection du drone** | Inspecter visuellement le drone (surtout les **hélices** = pas de pets & bien fixées) ||
| **Protection Gimbal** | **Retirer protection** du Gimbal ||
| **Caméra** | Inspection de la Caméra + vérifier la propreté de la lentille (nettoyer au chiffon doux) ||
| **Carte microSD** | Vérifier que la Carte microSD est bien positionnée ||
| **Smartphone** | Vérifier que le smartphone **bien chargé** et **connecté** + **luminosité de l’écran** est au max ||
| **Batterie** | Vérifier **l'insertion** & **la charge** de la batterie connectée sur le drone + niveaux de voltage des cellules ||
| **Antennes** | Positionner les antennes : Parallèles et angle adéquat par rapport à votre positionnement du drone ||
| **Calibrage du drone** | Calibrer la boussole (sauf si vous volez régulièrement au même endroit) ||
| **Interférences** | Retirer tout objet électronique ou métallique de vos poignets/poches (pour  limiter les interférences et ainsi éviter de parasiter votre calibration et votre boussole) + vérifier fréquences radio ||
| **Satellites** | Vérifier Satellites verrouillés (si P mode) ||
| **Altitude max** | Paramétrer l'altitude maximale de vol à **120 m** ||
| **Obstacles** | Observer les obstacles (arbres, bâtiments…) environnant et estimer leur hauteur ||
| **Altitude RTH** | Paramétrer une Altitude minimale pour le Retour à la Maison (RTH) supérieure aux obstacles environnants ||
| **RTH** | Vérifier que le point Home est bien été enregistré (et bien au bon endroit sur la carte !) ||
| **Décollage** | Vérifier que le lieu de décollage: suffisamment dégagé, des bonnes conditions générales (pas trop de monde), le drone à plat (ou sur un *landing pad*) ||
|  |  ||
|  |  ||
|  |  ||



## En vol



Sources:

* https://www.amateursdedrones.fr/ma-routine-pre-vol/
* https://lesvoyagesdetaco.fr/premier-vol-drone-check-list-debutant/



Pre-vol:



--. 
--
--

-

-Bonnes conditions pour le décollage
-

-Camera bien réglée pour la photo et/ou vidéo



En vol:

-Vol stationnaire (env. 1m = zone de turbulences au décollage) > écoutez le bruit des hélices et du moteur
-Vérifiez la puissance de votre signal à tout moment pendant le vol
-Pointez vos antennes dans la bonne direction en toutes circonstances !
-Gardez votre engin à vue,+ verifier signal en vol
-Pensez à taper l’écran pour l’autofocus
-Ne volez pas trop près de personnes ou animaux.