# Sac à dos photo



| Objet     | Descriptif                                                   | :notebook: Manuel                                          | Encombrement (mm) | Poids (g) |
| --------- | ------------------------------------------------------------ | :----------------------------------------------------------- | --------------- | --------- |
| **Sac à dos** | [Lowepro Flipside 400 AW](https://www.photoreview.com.au/reviews/accessories/lowepro-flipside-400-aw-camera-backpack/#In_Summary) | [![flipside400](images/flipside400.png)]() | 270 x 150 x 425 (int) / L303 x p253 x h460 (ext) | 1200 ou 1600  ?     |
||||||
| **Nikon D90** | [Boitier Nikon D90](https://www.nikon.fr/fr_FR/product/discontinued/digital-cameras/2015/d90#tech_specs) | [![nikonD90](images/nikonD90.png)](https://crossgate.nikonimglib.com/dsd_redirect/redirect.do?P=2fHSZ48&R=g4Jpl50&L=uYs6J01&O=kYWoz00) | 132 × 103 × 77 | 703       |
| Carte SD 8Go | [SanDisk - Carte SD Extreme III 8 Go Classe 6]() | [![sd8](images/sd8.png)]() | 32 x 24 x 2,1 | 2 |
| MicroSD 32Go + adaptateur | [Netac -  Carte microSD 32 Go U1]() | [![sd128](images/sd32.png)]() | 32 x 24 x 2,1 (avec adaptateur SD)  | 2 |
| Objectif 18-105  | [AF-S DX NIKKOR 18-105mm f/3.5-5.6G ED VR](https://www.nikon.fr/fr_FR/product/nikkor-lenses/auto-focus-lenses/dx/zoom/af-s-dx-nikkor-18-105mm-f-3-5-5-6g-ed-vr) | [![nikkor_18_105](images/nikkor_18_105.png)](https://crossgate.nikonimglib.com/dsd_redirect/redirect.do?P=8Z4Yp97&R=C76bm05&L=DP7h602&O=P2Go300) | h78 (sans PS) x d115 | 420       |
| Kit de Filtres 67mm | [Kit De Filtre K&F Concept 67mm UV CPL ND4](https://www.kentfaith.fr/filtres-67mm/SKU0012_filtre-objectif-kit-67mm-uv-cpl-nd4) | [![filtres_67](images/filtres_67.png)]() | 134 x 134 x 42 (colis) |        |
| Objectif 90 macro  | [Tamron SP AF 90mm F/2.8 Di MACRO 1:1](https://g.co/kgs/LTcZ2v) | [![tamron_90_macro](images/tamron_90_macro.png)](http://tamron.cdngc.net/inst/pdf/272eb01inst_1010_fr.pdf) | h84 x d120 | 405       |
| Objectif 150-600  | [Tamron SP 150-600mm F/5-6.3 Di VC USD](https://www.tamron.eu/fr/objectifs-photo/sp-150-600mm-f5-63-di-vc-usd/) | [![tamron_150_600.png](images/tamron_150_600.png)](http://tamron.cdngc.net/inst/pdf/a011inst_1311_fr.pdf) | h125 x d280 | 1951       |
| Objectif 50  | [AF-S NIKKOR 50mm f/1.8G](https://www.nikon.fr/fr_FR/product/nikkor-lenses/auto-focus-lenses/fx/single-focal-length/af-s-nikkor-50mm-f-1-8g?ID=6131081) | [![nikkor_50](images/nikkor_50.png)](https://crossgate.nikonimglib.com/dsd_redirect/redirect.do?P=0Lny476&R=TMGvG06&L=p0BeX02&O=tlOg400) | h78 x d83 | 185       |
| Chargeur  | [QuickChargeur MH-18a pour Nikon EN-EL3](https://store.nikon.fr/chargeur-de-batterie-mh-18a/VAK146EA/) | [![chargeur_d90](images/chargeur_d90.png)](https://crossgate.nikonimglib.com/dsd_redirect/redirect.do?P=0Lny476&R=TMGvG06&L=p0BeX02&O=tlOg400) | 88 x 58 x 34 + cable | 259      |
| Capture Clip  | [Peak Design Capture Clip V2]() | [![peakdesign_v2](images/peakdesign_v2.png)]() | 104 x 28 x 52 | 100 |
| Trépied  | [K&F SA255C1 Trépied Carbonne & rotule](https://www.kentfaith.fr/KF09.093_k-f-sa255c1-monopode-tr%C3%A9pied-professionnel-en-carbone-67-pouces-avec-rotule-%C3%A0-360-degr%C3%A9s?gclid=CjwKCAjwm7mEBhBsEiwA_of-TB-SJ12QkucFP6O4J6MM2AMxDwAdojR70028Z2PqavTgowF91uikEBoC4RMQAvD_BwE) | [![trepied_p5100](images/KFtrepied.png)]() | 13.4x13.4xh42 (plié) / h53-172 (déplié) | 1370 (dont rotule 262) |
| Trépied  | [Posso Trépied P5100 + adaptateur Arca swiss](https://fr.clasf.com/posso-p5100-tr%C3%A9pied-r%C3%A9glable-pour-appareil-photovideo-%C3%A0-rochefort-du-gard-17126203/) | [![trepied_p5100](images/trepied_p5100.png)]() | h46 (plié) / h120 (déplié) | 920 + 142 |
||||||
| **Osmo Action** | [DJI Osmo Action](https://www.dji.com/fr/osmo-action/info#specs) | [![osmo_action](images/osmo_action.png)](https://dl.djicdn.com/downloads/Osmo%20Action/Osmo_Action_User_Manual_v1.0_FR.pdf) | 65 x 42 x 35 | 124       |
| MicroSD 128Go | [Carte SanDisk Extreme microSD 128 Go U3 V30](https://store.dji.com/fr/product/sandisk-micro-sd-card-128gb) | [![sd128](images/sd128.png)]() | 15 x 11 x 1                     |      |
| Station de recharge | [DJI charging station Osmo Action](https://www.dji.com/fr/osmo-action/info#specs) | [![station_osmo_action](images/station_osmo_action.png)](https://dl.djicdn.com/downloads/Osmo%20Action/Osmo_Action_Charging_Hub_User_Guide.pdf) | 553 x 476 x 253 | 37,5       |
| Batteries | [DJI Batteries Osmo Action  1300mAh + boîtier](https://store.dji.com/fr/product/osmo-action-battery) | [![batt_osmo_action](images/batt_osmo_action.png)]() | 379x408x151 (batterie) + 188x490x446 (boîtier) | 2 x (30 + 10)     |
| Mini-perche-trépied | [Telesin mini  perche-trépied]() | [![mini_trepied](images/mini_trepied.png)]() | h125 (plié) / h235 (déplié) | 56,4 |
| Boîtier étanche | [Shoot Boîtier étanche 30m]() | [![boit_etanche](images/boit_etanche.png)]() | 100 x 94 x 66 (colis) | 200 (colis) |
||||||
| **Mini 2** | [DJI mini 2](https://www.dji.com/fr/mini-2/specs) | [![mini2](images/mini2.png)](https://dl.djicdn.com/downloads/DJI_Mini_2/20201130/DJI_Mini_2_User_Manual_EN.pdf) | 138 x 81 x 58 | 242     |
| MicroSD 128Go | [Carte SanDisk Extreme microSD 128 Go U3 V30](https://store.dji.com/fr/product/sandisk-micro-sd-card-128gb) | [![sd128](images/sd128.png)]() | 15 x 11 x 1 (+ adaptateur SD)                    |      |
| Radio commande | [DJI Controleur mini 2](https://www.dji.com/fr/mini-2/specs) | [![controleur_mini2](images/controleur_mini2.png)](https://dl.djicdn.com/downloads/DJI_Mini_2/20201130/DJI_Mini_2_User_Manual_EN.pdf) | 180 x 86 x 10 | 389     |
| Batteries de vol | [DJI Batterie de Vol Intelligente 2250 mAh](https://store.dji.com/fr/product/mini-2-intelligent-flight-battery) | [![batt_mini2](images/batt_mini2.png)]() | 75 x 40 x 37 | 2 x 86.2     |
| Chargeur 3A | [DJI Chargeur 18W 5V/3A](https://store.dji.com/fr/product/dji-18w-usb-charge) | [![chargeur_mini2](images/chargeur_mini2.png)]() | 85 x 40 x 22 |      |
| Station de recharge | [DJI Hub de charge  USB 18W + powerbank](https://store.dji.com/fr/product/mini-2-two-way-charging-hub) | [![hub_mini2](images/hub_mini2.png)]() | 120 x 73 x 32 |                 |
| Landing Pad | [PGYTECH - Tapis D’atterrissage Pliable ](https://www.pgytech.com/collections/for-dji-mini-2/products/landing-pad-pro-for-drones) | [![landing_pad](images/landing_pad.png)]()|262 x 174 x 25|352 / 441|
||||||
| **Batterie Externe** | [VIVIS Power Bank 20000 mAh](https://portablebatteries.fr/tests-avis/vivis-20000mah/) | [![powerbank_20k](images/powerbank_20k.png)]() | 142 x 86 x 17 + cables USB | 338   |
| **Chargeur 39W** | [Rampow -  Chargeur 39W 2x 5V/3A](https://www.amazon.fr/gp/product/B08FHY42VW/ref=ppx_yo_dt_b_asin_title_o00_s00?ie=UTF8&psc=1) | [![chargeur_39w](images/chargeur_39w.png)]() | 58 x 97,5 x 28,5 | 98 |
| **Jumelles 10x** | [Bushnell - Jumelles  10x 25mm EXplorer FOV 325ft | [![jumelles](images/jumellesB.png)]() | 130 x 122 x 50 | 242 |
| **Jumelles 8x** | [Tasco - Sonoma - Jumelles  8x 21mm FOV367ft/123m](https://www.opticsplanet.com/tasco-8x21-sonoma.html) | [![jumelles](images/jumellesT.png)]() | 100 x 60 x 35 | 139 |
||||||
| **Pince multifonction** | [Victorinox Swisss Tool]() | [![swisstool](images/swisstool.png)]() | 150 x 210 | 290 |
| **Lampe Frontale** | [PETZL - Zipka Lampe frontale 100lm]() | [![zipka](images/zipka.png)]() | 58 x 42 x 39 | 68 |
||||||
|**Smartphone**|[Samsung - S7 Edge](https://www.samsung.com/fr/support/model/SM-G935FZKAXEF/)|[![smartphone](images/smartphone.png)](https://org.downloadcenter.samsung.com/downloadfile/ContentsFile.aspx?CDSite=UNI_FR&OriginYN=N&ModelType=N&ModelName=SM-G935F&CttFileID=7031446&CDCttType=UM&VPath=UM%2F201805%2F20180504175845092%2FSM-G935_UM_Open_Oreo_Fre_Rev.1.0_180504.pdf)|152 x 78 x 10|152|
||||||
| **Coolpix s33 Rose** | [Nikon - CoolPix s33 Rose](https://www.nikon.fr/fr_FR/product/discontinued/digital-cameras/coolpix/2017/coolpix-s33#tech_specs) | [![coolpix_s33](images/coolpix_s33.png)](https://crossgate.nikonimglib.com/dsd_redirect/redirect.do?P=L2ap176&R=4MAld25&L=lMWyJ02&O=jx0Oc00) | 110 × 67 × 38 | 180       |

# Kayak

| Objet     | Descriptif                                                   | :notebook: Manuel                                          | Encombrement (mm) | Poids (g) |
| --------- | ------------------------------------------------------------ | :----------------------------------------------------------- | --------------- | --------- |
| **Twist 2/1** | [Gumotex Twist 2/1](https://www.gumotexboats.com/fr/twist-21#0000-045355-918-5C/11C) | [![twist2](images/twist2.jpg)](Manuel_Twist2.pdf) | 530 x 360 x 200 (plié) / L3600 x l830 (déplié) | 13000   |
| **Gilet Jobe TU** | [Gilet de flotabilité Jobe TU](https://www.nautigames.com/shop/ficheproduit/3640-284415-gilet-de-flottabilite-jobe-universal-rouge.htm) | [![jobe](images/jobe.png)]() | L000 x l360 x p200) | 000   |
| **Pagaie 4 parties fibre** | [AquaMarina Pagaie 4 parties fibre](https://www.nautigames.com/shop/ficheproduit/3640-284415-gilet-de-flottabilite-jobe-universal-rouge.htm) | [![AMpagaie](images/AMpagaie.png)]() | L2300 x diam29 (montée) /4 ) | 1100  |

