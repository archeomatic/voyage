# Matériel électro

Matériel électronique pour voyage en Camping-Car à ajouter à la liste **[Sac à dos photo](sac_photo.md)**



| Objet     | Descriptif                                                   | :notebook: Manuel                                          | Encombrement (mm) | Poids (g) |
| --------- | ------------------------------------------------------------ | :----------------------------------------------------------- | --------------- | --------- |
| **Vidéo-projecteur** | [Tenswall - Vidéoprojecteur Portable](https://www.amazon.fr/gp/product/B07BXMT36M) | [![videoproj](images/videoproj.png)]() | 145 x 80 x 18 | 286    |
| Accessoires Vidéoproj | [Chargeur+cable / Télecommande / Cable HDMI]() | |  |     |
| Drap de projection | [Drap de projection]() | |  |     |
| **MiraCast** | [Miracst - MiraScreen 2.4G WiFi Display Dongle](https://www.amazon.fr/gp/product/B071RTDJ8X) | [![miracast](images/miracast.png)]() |  |    |
| **Enceinte BT** | [Anker - Soundcore Motion+ Enceinte Bluetooth 30W](https://www.amazon.fr/gp/product/B07P39MLKH) | [![soundcore](images/soundcore.png)]() |  |   |

