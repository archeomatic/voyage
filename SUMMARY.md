# Listes de voyages



## Indispensables


* :baggage_claim: [Sac à dos: matos photo/vidéo/+](listes/sac_photo.md)

## Camping-Car


* :electric_plug: [Matos eléctro en camping-car](listes/electro.md)

## BackPack

* (à venir) listes de voyage en sac à dos

## Mémos

* :page_facing_up: [Check-list mini 2](memo/check_list_mini2.md)
* :camera: [rappels photo et prises de vues](memo/memo_photo.md)
* :helicopter: +  :camera: [drone & photographie](memo/memo_photo_mini2.md)
* :helicopter: + :camera: + :earth_africa:  [drone & photogrammétrie](memo/memo_phg_mini2.md)
* :helicopter: + :camera: + :earth_africa:  [OpenDroneMap](memo/opendronemap.md)
* :helicopter: + :movie_camera: [drone & vidéo](memo/memo_video_mini2.md)


## Papiers

*  :passport_control: [Papiers](https://drive.google.com/drive/folders/1h4-nZik3-qhAk21FjKPyihkLn8t8XGv3?usp=sharing)

## Règles de Jeux

*  :heart: [Love Letter](jeux/loveletter.md)
*  :train: [Flip 9](jeux/flip9.md)
*  :tea: [Eleveneses](jeux/elevenses.md)
*  :skull: [The Game](jeux/thegame.md)
*  :dizzy_face: [The Mind](jeux/themind.md)

* :cow: [6 qui prend !](jeux/6quiprend.md)

## Switch

*  :video_game: [Switch Jailbreak](jeux/jailbreak.md)