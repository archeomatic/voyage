# Flip 9 - Un voyage en train
![flip9](images/flip9/flip9.png)   

**Type :** Jeu de placement de cartes.

**Nombre de joueurs :** 1 joueur

**Âge:** 4+

**Durée:** 15 secondes... ou plus.

**But :** Aligner les cartes de 1 à 9 (sur les faces de la même couleur/même paysage)

**Matériel :** 9 cartes numérotées de 1 à 9 sur les 2 faces (face paysage de montagne vert et face payasage urbain en niveaux de gris).




>« Tchou-tchou » Le train file dans la vallée, mais... Ouch il y a un problème là devant
>!
>Utilisez votre intelligence pour réarranger la séquence de cartes pour que le train
>puisse traverser en toute sécurité la forêt et la grande ville, pour enfin vous
>conduire au point d'arrivée !



## I – Partie de découverte

1. Placez au hasard les 9 cartes pour former une rangée de carte avec la même face (paysage de montagne vert) visible.
2. A chaque tour, vous devez échanger de place deux cartes: Additionnez les nombres des deux cartes échangées ; si la somme dépasse 9 ; ajoutez le chiffre de la dizaine à l'unité de la somme obtenue.
3. Une des cartes échangées, à chaque tour, doit correspondre au nombre final obtenu au tour précédent, comme expliqué à l'étape 2 *(exception : au premier tour, vous pouvez prendre n'importe quelles cartes).*
4. Répétez les étapes 2 et 3. Lorsque vous arrivez à ce que toutes les cartes soient dans l'ordre croissant **(de 1 à 9, de la droite vers la gauche)**, vous avez gagné !

## II – Version Standard

Les règles sont les mêmes que pour les parties de découvertes à l'exception des
différences suivantes.

1. Dans l'étape 1, **NE mettez PAS les cartes sur la même face.** Choisissez au hasard la face sur laquelle vous mettez les cartes.
2. Dans l'étape 2, lorsque vous échangez deux cartes, vous retournez aussi les deux
cartes sur l'autre face.
3. En plus de créer une séquence de nombres dans l'ordre croissant, vous devez aussi
faire que les cartes soient toutes de la même face pour gagner la partie.

## VI – Sources 

**Règle du jeu :** inspirée/pompée sur [BGG](https://boardgamegeek.com/boardgame/165737/flip-9)
**Auteur et Illustration d'origine:** Chen, Chin-Fan
**Version pnp:** personnelle

