# Love Letter 
![Love Letter : logo](images/loveletter/loveletter.png)   

**Type :** Jeu de cartes minimaliste à identité cachée.

**Nombre de joueurs :** 2-4 (jusqu’à 8  avec les extensions non-officielles).

**But :** Terminer la manche en ayant en main la carte avec le rang le plus élevé. Le joueur gagne alors un *jeton cœur*.

**Vain*cœur* :** Le joueur avec le plus de cœurs gagne la partie (voir nombre de cœurs à atteindre au Chapitre III).

**Fin d’une manche :** La dernière carte est piochée (ou tous les joueurs sont éliminés sauf 1).

**Matériel :** 16 cartes + des jetons cœur.



## I – Mise en place de la manche

1. **Mélangez les 16 cartes**. Vous obtenez la pioche.

2. **Retirez la première carte du jeu**, sans la regarder (vous jouerez la manche sans cette carte).

   *Note: Dans le cas d’une partie à **2 joueurs** : piochez **3 cartes** et disposez les face visible sur la table, elles ne seront pas utilisées pendant cette manche.*

3. Chaque joueur **pioche 1 carte** et la garde secrète.

------

## II – Déroulement d’une manche

1. **Piocher 1 carte :** Le premier joueur pioche une carte et l’ajoute à sa main.

2. Défausser une carte :

    Il choisit une de ses cartes, la défausse en la posant devant lui, face visible et en applique les effets (voir la liste des cartes). 

   - Les cartes jouées restent devant chacun des joueurs dans l’ordre où elles ont été jouées.
   - Une fois les effets de la cartes appliqués, c’est au joueur suivant de jouer.
   - Si un joueur est éliminé de la manche, il défausse sa main face visible, mais ne pioche pas de nouvelle carte.

------

## III – Fin de manche et fin de jeu

**Fin d’une manche :**
Lorsqu’un joueur pioche la  dernière carte, il joue son tour et la manche se termine (La manche se  terminé également si tous les joueurs sauf un ont été éliminés).
**En cas d’égalité :** Chaque joueur totalise les valeurs des cartes de sa défausse. Celui qui obtient la plus grande valeur gagne la manche. Au bout d’un certain  nombre de manches, vous aurez cumulé suffisamment de cœurs pour mettre  fin au jeu. Le total des cœurs à atteindre varie selon le nombre de  joueurs.
S’il y a de nouveau égalité entre certains joueurs, ces joueurs gagnent chacun un cœur.
**Recommencer une nouvelle manche :** Le vainqueur de la manche précédente devient premier joueur de la  nouvelle manche. S’il y avait égalité, celui qui commence est le dernier à avoir eu un rendez-vous amoureux.

**Fin du jeu – Nombre de cœurs à atteindre selon le nombre de joueurs :**

- 2 joueurs = 6
- 3 joueurs = 5
- 4 joueurs = 4
- 5 joueurs : 3
- 6 joueurs : 3

------

## IV – Les 8 cartes du jeu

Voici les **8 cartes** du jeu et leurs **effets**. Les cartes sont rangées selon l’ordre croissant de leur **rang**. Du Garde (rang 1) à la Princesse (rang 8). Certaines cartes existent en plusieurs exemplaires.



1. **Garde (x5) :** Choisissez un joueur et essayez de deviner la carte qu’il a en main (excepté le Garde), si vous tombez  juste, le joueur est éliminé de la manche.
2. **Prêtre (x2) :** Regardez la main d’un autre joueur.
3. **Baron (x2) :** Vous et un autre joueur comparez secrètement vos mains. Le joueur qui a la carte avec la plus faible valeur est éliminé de la manche.
4. **Serveuse (x2) :** Jusqu’au prochain tour, vous êtes protégé des effets des cartes des autres joueurs.
5. **Prince (x2) :** Choisissez un joueur (y compris vous), celui-ci défausse la carte qu’il a en main pour en piocher une nouvelle.
6. **Roi (x1) :** Échangez votre main avec un autre joueur de votre choix.
7. **Comtesse (x1) :** Si vous avez cette carte en main en même temps que le King ou le Prince, alors vous devez défausser la carte de la Comtesse.
8. **Princesse (x1) :** Si vous défaussez cette carte, vous êtes éliminé de la manche.

![Love Letter : perso](images/loveletter/Loveletter_persos.png)

## V – Autres personnages 

**Chancelier (Valeur 6) :** Piochez 2 cartes du paquet et ajoutez-les à votre main. Choisissez et conservez **une** des trois cartes de votre main, puis placez les deux autres, faces cachées au dessous du paquet (dans l’ordre de votre choix).
S’il ne reste qu’une seule carte dans le paquet, piochez-la et remettez-en  ne à sa place. Si le paquet est épuisé, le Chancelier n’a pas d’effet  quand il est joué.

**Espionne (Valeur 0) :** Une Espionne n’a pas d’effet actif lorsqu’elle est jouée ou défaussée.
A la fin de la manche, si vous êtes **le seul joueur encore en lice** qui a joué ou défaussé une Espionne, vous gagnez 1 pion de Faveur.
Cela ne revient pas à remporter la manche : le vainqueur (même si c’est vous) gagne quand même son pion Faveur.
Vus ne gagnez toujours qu’un seul pion, même si vous jouez et/ou défaussez les deux Espionnes.

## VI – Sources 

**Règle du jeu :** inspirée/pompée sur https://www.regledujeu.fr/love-letter/

**Illustrateur:** https://yohanbourgeois28.wixsite.com/home/loveletter

**Version pnp:** adaptée d'après la version misen ligne par *[Gus & Co](https://gusandco.net/2013/12/08/love-letter-une-version-hallucinante-mise-a-jour/)*

