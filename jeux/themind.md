# The mind
![the mind: logo](images/mind/themind.png)   

**Type :** Jeu de cartes coopératif basé sur l'entente entre joueurs.

**Nombre de joueurs :** 2-4 

**Durée :** 15 mn

**But :** Les joueurs doivent rassembler toutes les cartes qu'ils ont en main dans l'ordre croissant au milieu de la table, une par une. MAIS, vous n'êtes pas autorisé à révéler quoi que ce soit à propos de vos propres cartes !

**Fin :** Si les joueurs ont terminé tous les niveaux, c'est la victoire !

**Matériel :** 100 cartes numérotées de 1 à 100 + carte indiquant le niveau en cours / les shuriken et les vies.



## I – Mise en place

La mise en place est très rapide : mélangez bien le paquet de cartes. Sur la carte de comptage , placez le nombre de points de vie en fonction du nombre de joueurs et de shuriken en fonction du nombre de joueurs également. 

| 2  joueurs    | Niveaux 1 à 12     | 2 Vies     | 1 Shuriken     |
| ------------- | ------------------ | ---------- | -------------- |
| **3 joueurs** | **Niveaux 1 à 10** | **3 Vies** | **1 Shuriken** |
| **4 joueurs** | **Niveaux 1 à 8**  | **4 Vies** | **1 Shuriken** |


------

## II – Règles de The Mind

On commence avec le premier niveau, **chaque  joueur se voit remettre une seule carte qu'il ne faut pas montrer aux  autres joueurs**. **Pour terminer le niveau, il faut que tous les joueurs aient posé leur carte dans l'ordre croissant**.

N'importe quel joueur peut poser sa carte au centre de la table s'il  pense que sa carte est celle de la plus faible valeur parmi tous les  joueurs. Lorsqu'un joueur pose une carte, tous les autres vérifient s'ils ont une carte de plus faible valeur. Si c'est le cas, alors les  joueurs perdent une vie et peuvent défausser toutes les cartes de plus  faible valeur que celle qui a été jouée. Attention, afin de ne pas exploiter le système, un joueur doit toujours jouer sa carte la plus faible en priorité.

Les joueurs vont ainsi poser les cartes les unes sur les autres.  Lorsque les joueurs ont posé toutes leurs cartes, le niveau est terminé, et parfois un shuriken ou point de vie bonus est gagné (selon le niveau terminé). Au début de chaque niveau, on distribue autant de cartes que  le niveau actuel (2 cartes au niveau 2, 3 cartes au niveau 3...) à  chaque joueur.

**À n'importe quel comment, un joueur peut lever la main**. Si les autres joueurs lèvent la main en signe d'approbation, alors **l'équipe utilise  un shuriken**. Chaque joueur va défausser la carte la plus faible qu'il a  en main. La partie reprend ensuite normalement.

Si les joueurs perdent leur dernier point de vie, c'est la défaite !  Si les joueurs ont terminé tous les niveaux, c'est la victoire !

**Il est bien évidemment interdit de parler pour donner des indices ou  des indications pendant la partie**, par exemple en tapant sur la table ou en faisant des signes des yeux.

Il faut également éviter de compter, en secondes par exemple, même dans sa tête. Le but est d'essayer d'avoir un rythme d'équipe et une  bonne coordination, plutôt que d'être bien calé sur les secondes...

## III – Sources 

**Auteur :** [Wolfgang Warsch](https://www.trictrac.net/jeu-de-societe/liste/auteur-illustrateur/wolfgang-warsch)

**Règle du jeu :** https://www.trukmuchspot.fr/regles-et-critiques/the-mind

**Version pnp: **personnelle, créée avec la fonction Atlas de QGIS. 

