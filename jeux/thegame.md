# The game
![the game : logo](images/game/thegame.png)   

**Type :** Jeu collaboratif de défausse de cartes.

**Nombre de joueurs :**1-5 

**Durée :** 20 mn

**But :** Se débarrasser de toutes les cartes sans jamais être bloqué.

**Fin :** Si tous les joueurs ont réussi à se débarrasser de leurs cartes, bravo  vous avez gagné ! Sinon le nombre de cartes restant dans le deck et dans les mains des joueurs représente votre score. Essayez de faire encore  moins la prochaine fois !

**Matériel :** 100 cartes numérotées de 2 à 99 + 2 cartes ascension (1→99) et descente (100→2).



## I – Mise en place

La mise en place est très simple, il suffit de poser les 2 cartes ascension (1→99) et descente (100→2) au milieu de la table. Distribuez 6 à 8 cartes à chaque joueur selon le nombre total de joueurs. Mettez le deck à  portée de main de tout le monde. Et voilà, vous êtes prêts à jouer !

| 1 joueur          | 8 cartes            |
| ----------------- | ------------------- |
| **2 joueurs**     | **7 cartes chacun** | 
| **3 à 5 joueurs** | **6 cartes chacun** |



------

## II – Règle simplifiée de The Game

* Les joueurs décident ensemble qui commence. On joue ensuite dans le sens des aiguilles d’une montre.
* Le joueur dont c’est le tour doit poser **au moins 2 cartes** (ou plus) de sa main sur n’importe quelle pile, puis il pioche le même nombre de cartes.
* **La valeur des cartes** des deux piles à droite et à gauche de la **carte ascension (1→99)** doit **toujours être supérieure à la dernière carte posée** *(par exemple 3, 11, 12, 13, 20, 34,...)*.
  **La valeur des cartes** des deux piles à droite et à gauche de la **carte descente (100→2)** doit toujours être inférieure à la dernière carte posée (94, 90, 78, 61, 60, 57,...).
* ***Le rétro :*** Dans les piles ascension, une carte dont **la valeur est exactement inférieure de 10** à la valeur de la dernière carte peut être posée sur chaque pile. Evidemment l'effet retro fonctionne sur les piles descendentes avec une /des cartes dont la valeur est exactement supérieur de 10 à la valeur de la dernière carte posée. **Un joueur peut utiliser le rétro plusieurs fois** pendant son coup, même sur différentes pile.
* **Quelques interdictions sont données afin de ne pas rendre le jeu trop facile :**
  * Ne communiquez pas tout et n'importe quoi ! Il est bien entendu  **interdit de parler de nombres ou de montrer vos cartes aux autres  joueurs**. La seule chose qui est autorisée est de demander à ce que l'on  réserve une pile pour soi, ou de ne pas trop monter/descendre.
  * Il est **interdit de regarder quelles sont les cartes qui ont déjà été  posées** et qui sont dans la pile. Vous êtes censés ne voir que la carte  du haut !

Si tous les joueurs ont réussi à se débarrasser de leurs cartes,  bravo vous avez gagné ! Sinon le nombre de cartes restant dans le deck  et dans les mains des joueurs représente votre score. **Moins de 10 cartes est un excellent résultat. Si les 98 cartes ont été jouées, la victoire est totale !** 

------

## III – Extension: The Game On Fire

<img src="images/game/icon_fire.png" alt="icon_fire" style="zoom:8%;" />

Quand quelqu'un joue une carte "Incendie" (les 6 cartes avec un numéro "double": 22, 33, 44, 55, 66, 77, 88) cette carte doit être  couverte avant la fin du tour, sinon la partie  est immédiatement perdue !                          

------

## IV – Extension: The Game Extrême

Les règles du jeu original ne changent pas et restent valables. Le déroulement et la préparation du jeu sont également les mêmes : chacun reçoit 6 cartes (pour 3, 4 ou 5 joueurs), 7 cartes pour 2 joueurs et 8 cartes si on joue seul. Celui dont c’est le tour doit toujours jouer au moins 2 cartes. Si la pioche est épuisée, on peut jouer seulement 1 carte.
Les **28** cartes **consignes** sont nouvelles (7 différentes, 4 exemplaires de chacune).
Si le joueur dont c’est le tour joue une carte avec une consigne, il **doit** dans tous les cas respecter **exactement** cette consigne, sinon le jeu est immédiatement perdu. Le joueur dont c’est le tour peut jouer plusieurs cartes consignes pendant son coup, mais **toutes** ces consignes doivent alors être strictement respectées.
Il est aussi permis de communiquer entre les joueurs sur les consignes, par exemple de dire quelles sont les consignes dans sa main et lesquelles il vaut mieux s’abstenir de jouer à ce moment là. Toute communication concernant des valeurs
concrètes reste taboue.

### A - Les 3 symboles éclair (triangle orange)
***Les consignes avec un symbole éclair*** doivent être exécutées **immédiatement**. Elles sont toujours valables unibuement pour le joueur actif qui les pose pendant son coup. quand le joueur **actif** a terminé son coup, les consignes avec un symbole éclair posées n’ont plus aucune importance.

<img src="images/game/stop.png" alt="stop" style="zoom:25%;" />

Si le joueur actif joue une **carte stop**, son action prend fin immédiatement. Le joueur actif peut jouer cette carte immédiatement comme première carte. Son action est alors terminée avec cette première carte.

<img src="images/game/mort.png" alt="mort" style="zoom:25%;" />

Le joueur actif doit recouvrir une **carte tête de mort** posée au cours **du même coup**. Aucune carte tête de mort ne doit donc rester visible sur le dessus d’une pile à la fin d’un coup.
**Attention :** si les joueurs devaient réussir à poser la totalité des I8 cartes sur les piles et si une carte tête de mort posée (au cours du tout dernier coup) reste alors visible, le jeu est perdu malgré tout.

<img src="images/game/3cartes.png" alt="3cartes" style="zoom:25%;" />

Le joueur actif doit jouer au total exactement 3 cartes pendant son coup. Il est permis de jouer une carte stop comme troisième carte (et unibuement pour la troisième carte b) . *Exemple : Tim joue d’abord une carte numérotée normale, puis la carte „3!“, enfin une carte stop. Il a posé exactement 3 cartes comme requis et pioche 3 cartes.*
**Attention :** si à la fin de la partie, le dernier joueur devait seulement encore avoir une ou deux cartes dans sa main au début de son coup, il peut certes jouer une carte „3!“, mais le jeu est considéré comme perdu parce bue la consigne ne peut pas être respectée exactement.

### B - Les 4 symboles infini (fond bleu)

***Les consignes avec un symbole infini*** sont applicables précisément à partir du moment où elles sont posées. Elles restent valables **pour tous les joueurs** tant qu'elles sont **visibles** sur le dessus d’une pile. Si plusieurs consignes permanentes de ce type sont visibles sur différentes piles, elles restent toutes valables. quand une consigne permanente est recouverte d’une autre carte, elle
n’a plus aucune importance ensuite.

<img src="images/game/silence.png" alt="silence" style="zoom:25%;" />

Les joueurs ne peuvent plus communiquer entre eux (concernant le jeu), donc ni parler du jeu, ni se faire des signes quelconques.

<img src="images/game/noretro.png" alt="noretro" style="zoom:25%;" />

Le joueur actif ne peut plus utiliser de rétro. Ceci est valable pour les 4 piles.

<img src="images/game/pile.png" alt="pile" style="zoom:25%;" />

Le joueur actif doit poser toutes les cartes qu'il joue pendant son coup sur une seule pile. Il est libre de choisir la pile.

<img src="images/game/1carte.png" alt="1carte" style="zoom:25%;" />

À la fin de son action, le joueur actif ne peut piocher qu'une seule carte, peu importe le nombre de cartes qu'il a posées. Il se peut donc alors qu'il ait moins de cartes bue la normale dans sa main.
Quand la carte consigne est recouverte, le joueur actif peut, immédiatement après son action, piocher pour revenir au nombre de cartes initial (8 cartes pour un jeu en solitaire, 7 cartes à deux, 6 cartes à 3, 4 ou 5 joueurs)

## V – Sources 

**Auteur :** [Steffen Benndorf](https://www.trictrac.net/jeu-de-societe/liste/auteur-illustrateur/steffen-benndorf)

**Règle du jeu :**

*  The Game: [règle officielle en pdf](http://jeuxstrategie1.free.fr/jeu_the_game/regle.pdf)
* The Game On Fire: https://www.trictrac.net/forum/sujet/the-game-on-fire-regle-en-vf
* The Game Extrême: https://nsv.de/wp-content/uploads/2018/05/the-game-extreme-f.pdf

**Version pnp: **personnelle, créée avec la fonction Atlas de QGIS.

