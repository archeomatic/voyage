# Jailbreak de la switch (15.0.1 )

:warning: Si le jailbreak a déjà été effectué une fois, pour le réinjecter passer directement au 4. et 5.

## 1.Vérifier la compatibilité de la switch

en reportant le numéro de série sur le site https://ismyswitchpatched.com/

## 2. Rassembler le matériel et logiciels

* Prendre un câble :video_game: USB-C - :computer: USB et vérifier qu'il permet le transfert des données

> * Brancher le câble entre le PC et la console.
>
> * :video_game: paramètres de la console → Gestion des données → Gestion des captures d'écran et vidéos → Copier sur un ordinateur via la connexion USB → Message "Connexion établie avec l'ordinateur" = :heavy_check_mark:

* Prendre une carte microSD UHS-I avec une vitesse de transfert de 60 à 95 Mo/seconde.
* Un RCM Jig for switch ou en fabriquer un avec du papier aluminium (cf. la [vidéo](https://youtu.be/2woLBKQ6HkE?t=373) à 6:13)
* :computer: Télécharger le [dossier](https://www.youtube.com/redirect?event=video_description&redir_token=QUFFLUhqbGVBNnlNTGdDUnNRRkFEQU9aWmgwUHI2MFViZ3xBQ3Jtc0tuWk1KaEhPX1BOMWJlYmlQRmVIaEpHNFRWNklLUkRlNklIYzNtTmFXTWY0MVNxYnRlYmpFMmtOaFR3anBUSUM0SVd4Qk9Kdy1DWGxXZTN6djBlTkluOXNsUFNZZnRnX1BDaVZFdk1Ubkk2VVJQUVJBVQ&q=https%3A%2F%2Fwww.mediafire.com%2Ffile%2Fk4t4bwbrwaryatw%2FJailbreak%2BSwitch%2B15.0.1.zip%2Ffile&v=2woLBKQ6HkE) contenant tout le nécessaire

> Une fois décompressé le dossier contient les logiciels nécessaires à installer sur le PC et les dossier à copier sur la carte microSD.

## 3. Préparer la cartes SD et le PC 

* :video_game: Formater la carte SD (depuis la console de préférence).
* :computer: Copier tous les dossiers du sous dossier :file_folder: *Jailbreak Switch\Dans la carte SD* sur la carte microSD  

![dossiers_sd](image/dossiers_sd.png)

## 4. Recovery Mode :video_game:

* Eteindre complétement la switch

> Pour éteindre la console (et non pas une simple mise en veille) : appui long sur le bouton power→ Options d'alimentation → Eteindre.

* Insérer la carte microSD préalablement formatée et contenant tous les dossier et fichiers nécessaires.

* Avec le **RCM jig en place**, appuyer simultanément sur **volume haut & power** :

→ :warning: la console ne doit pas s'allumer sinon éteindre la switch, replacer le jig et recommencer.

## 5. Injecter le jailbreak :computer:→:video_game:

* :computer: Depuis le dossier  :file_folder: *Jailbreak Switch\Pour le pc*, lancer le logiciel ![icon_RCMtegra](image/icon_RCMtegra.png)**TegraRcmGUI.exe**

   * A la première utilisation, il faut installer le driver USB.

  >  onglet [Settings] → [Install Driver] → [Suivant]→[Terminer]

  * Onglet [Payload] → Depuis le sous-dossier  :file_folder: *Jailbreak Switch 15.0.1\Pour le pc*, Charger le fichier **hekate_ctcaer_6.0.1.bin**

![tegra_payload](image/tegra_payload.png)

* Brancher la switch préalablement mise en RCM (Recovery Mode)

![tegra_inject](image/tegra_inject.png)

## 6. Paramétrer la console jailbreakée :video_game:

  cf. [vidéo](https://youtu.be/2woLBKQ6HkE?t=782) à 13:02

:warning: à ne faire que si cela n'a pas déjà été fait.



## 7. Installer un jeux avec :fire: Goldleaf

* Depuis le [dépôt Github](![icon_quark](image/icon_quark.png)):

  *  Télécharger :fire:**Goldleaf.nro** et le copier dans le dossier :file_folder:switch de la carte microSD 
  *  Télécharger ![icon_quark](image/icon_quark.png)**Quark.jar** sur le PC

  > consulter le site https://www.cfwaifu.com/goldleaf-quark/ pour plus d'info 

* Brancher le PC :computer: et la console switch :video_game: via le câble USB-C - USB

* :video_game: Sur la switch → cliquer sur l'icône Album → :fire: Goldleaf

* Exécuter le logiciel ![icon_quark](image/icon_quark.png)**Quark.jar**

  > Si cela n'a pas déjà été fait définir le répertoire qui contient les fichiers de jeux .nsd en cliquant sur le bouton [Add new path]

​	![quark](image/quark.png)

* :video_game: dans :fire:Gameleaf: Explorer le contenu → PC distant (via USB) → le dossier défini dans quark doit apparaitre: sélectionnez-le.
* Cliquer sur le fichier.nsp à installer → [Installer] → [carte SD] → [Installer] → :clock1030:
* Répéter avec les éventuels fichiers.nsp d'update et de DLC.