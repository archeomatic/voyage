#    Elevenses for One
![Elevenses for one : logo](images/eleven/elevenses.png)   

**Type :** Jeu de placement de cartes.

**Nombre de joueurs :** 1

**Âge:** 10+

**Durée:** 5 mn

**But :**  Si vous arrivez à placer les cartes de la Réserve sur le Chariot à Thé en une suite de 1 à 11 avant que ne sonne 11h, vous gagnez la partie. Si 11h sonne et que toutes les cartes ne sont pas sur le Chariot, vous perdez et devez essayer à nouveau de peur de perdre votre place chez Lady Agatha!

**Matériel :** 11 cartes Réserve, 2 cartes Temps, 1 carte Résumé.



> Vous êtes Grosvenor, le majordome de Lady Agatha Smythe, l'une des plus riches londoniennes du  quartier. Elle reçoit ses amies à 11 heures - mais, oh dear! Vous n'avez que 15 minutes pour tout  préparer et où est ce sucre? Vous ne devez pas être en retard! La réputation de Madame est en jeu!
> Bienvenue à Elevenses for One - un jeu solitaire où vous devez préparer tous les éléments  nécessaires pour un thé du matin splendide ... avant que l'horloge ne sonne 11 heure!

## I – Mise en place de la manche

1. Placez la carte Temps avec le pendule sur la carte Temps avec les heures pour ne laisser  apparaître que 10h45 (vous glisserez la carte vers le bas pour simuler le passage du temps).
2. Placez la carte « Chariot à Thé » face vers le haut, à gauche devant vous.
3. Mélanger les 10 cartes Réserve restantes et placez les, face vers le haut, sur une seule rangée à  droite de la carte « Chariot à Thé ». Cette rangée s'appelle la Réserve.

## II – Tour de jeu

![tour de jeu](images/eleven/tour.png)

## III – Les Cartes

||||
|---|---|---|
|Le chariot à thé (1)|![1](images/eleven/1.png) |Mettre cette carte sur le côté au début de la partie. Votre objectif est de placer les autres cartes de la Réserve sur elle. (Ne pas mélanger cette carte avec les autres).|
|Le thé (2)|![2](images/eleven/2.png)| Après avoir utilisé ou placé cette carte sur le chariot, choisissez n'importe quelle carte face vers le haut de la Réserve et retournez la face cachée sans réaliser son action. Si il n'y a pas de carte face vers le haut disponible, ne faites rien.|
|Le lait (3)|![3](images/eleven/3.png)|Après avoir utilisé ou placé cette carte sur le chariot, choisissez deux cartes, face vers le haut, de la Réserve et retournez les face cachée sans réaliser leurs actions. Si il n'y a qu'une carte face vers le haut disponible, vous devez la retourner. S'il n'y en a pas, ne faites rien.|
|Le sucre (4)|![4](images/eleven/4.png)| Après avoir utilisé ou placé cette carte sur le chariot, il n'y a pas d'action à réaliser. Cependant vous devez perdre 2 minutes au lieu d'une seule.|
|Les tasses à thé (5)|![5](images/eleven/5.png)|Cette carte ne peut jamais être défaussée. Après avoir utilisé ou placé cette carte sur le chariot, il n'y a pas d'action à réaliser. Cette carte ne peut pas être retournée face cachée par les cartes (2), (3) ou (9).|
|La porcelaine (6)|![6](images/eleven/6.png)|Après avoir utilisé ou placé cette carte sur le chariot, prenez toutes les cartes de la Réserve (sans les cartes défaussées), mélangez les et recommencez à la phase 1.|
|Les biscuits (7)|![7](images/eleven/7.png)| Après avoir utilisé ou placé cette carte sur le chariot, sélectionnez deux autres cartes face vers le haut de la Réserve et échangez leurs positions. Les deux cartes doivent rester face vers le haut et n’interagissent pas avec les autres cartes. S'il ne reste que deux cartes, échangez les. S'il y en a moins de deux, ne faites rien.|
|Les sandwiches (8)|![8](images/eleven/8.png)| Après avoir utilisé ou placé cette carte sur le chariot, prenez la première carte de la Réserve (face vers le haut ou cachée) et placez la face vers le haut à la fin de la rangée de la Réserve.|
|Les gâteaux (9)|![9](images/eleven/9.png)|Après avoir utilisé ou placé cette carte sur le chariot, retournez la prochaine carte après celle ci face cachée. Si la prochaine carte est déjà face cachée, ou si c'est la carte (5), ne faites rien. Si (9) est la dernière carte de la Réserve, ne faites rien.|
|Le serveur (10) |![10](images/eleven/10.png)|Cette carte ne peut jamais être défaussée. Après avoir utilisé cette carte, vous pouvez prendre une carte de la défausse et la placer face vers le haut directement après (10) dans la Réserve. Si vous faites cette action, retournez (10) face cachée mais ne perdez aucune minute (les serveurs sont tellement utiles!). Si vous utilisez cette carte mais ne pouvez (ou choisissez) de ne pas prendre de carte de la défausse, retournez la face cachée et perdez une minute. Quand vous placez (10) sur le chariot, perdez une minute (comme d'habitude), et si (11) est dans la défausse, vous devez la   remettre dans la Réserve, face vers le haut.|
|Elevenses (11) |![11](images/eleven/11.png)|Cette carte n'a pas d'action. Vous pouvez la retourner et perdre une minute sans effet. Cependant si vous décidez de défausser cette carte, vous pouvez d'abord ,si la situation vous le permet, placer une carte de la défausse sur le chariot puis poser (11) dans la défausse. Réalisez alors l'action de la carte placée et perdez une minute (ou deux si c'est (4)).|

## III – Remarques :

Parfois les instructions d'une carte ne peuvent pas être suivies. Dans tel cas, il n'y a pas de
d'actions à faire après avoir choisi 3A ou 3B. Cependant vous devez retourner la carte et perdre une minute pour avoir « utilisé » la carte. De plus si les instructions d'une carte peuvent être suivies, elle doivent être suivis à la lettre à chaque fois. Les seules exceptions sont les cartes (10) et (11). les instructions de ces cartes sont facultatives.
Souvenez vous que pour placer une carte sur le chariot il faut suivre les chiffres dans l'ordre croissant de 1 en 1.
Vous pouvez regarder votre défausse quand vous le souhaitez (en fait il est conseillé de laisser les cartes défaussées face vers le haut sur une rangée séparée de celle de la Réserve). Ces cartes ne peuvent pas revenir dans la Réserve (et donc être placées sur le Chariot) à moins qu'une carte ne vous le permette. De même les cartes défaussées ne sont pas remélangées avec celles de la Réserve à l'épuisement de celle ci.

## IV – Conditions de victoire

Une fois la partie terminée, soit par une réussite (placer toutes cartes avant 11h) soit par un échec, Lady Agatha évalue vos qualité de majordome selon les critères suivant :
** Le chiffre de la carte posée en dernier sur le chariot + Le nombre de minutes restantes:**
* 0-7 points : vous êtes congédié ! Vous faîtes honte à votre profession
* 8 – 10 points : vous avez déçu Madame... faîtes mieux la prochaine fois !
* 11 points : Vous avez rempli votre devoir sans montrer de réel talent.
* 12-13 points : Madame vous félicite et vous offre une demi journée de congé.
* 14-15 points : Devant votre dévouement, Madame vous augmente. Elle tient à vous garder (la coquine ;) )

## V – Besoin de challenges ?

Une fois que vous maîtrisez les cartes, débutez la partie en ajoutant à la Réserve le Chariot à Thé.
Vous pouvez également décider de débuter votre service quelques minutes plus tard...

## VI – Conseils

Gardez un œil sur tous les éléments présents dans la Réserve. Quelle carte vous permettra de déplacer une autre ? Quelle carte feriez vous mieux de défausser (sans perdre de minutes) ou de garder à porter de main ?
La chance est un facteur présent dans ce jeu mais garder un œil sur tout permet d'augmenter vos chances de succès. Après tout, Lady Agatha compte sur vous !

*« Tea is served, Madam. »*

## Sources

**Créateur:** David Harding
**Illustrateur:** [TJ Lubrano](www.tjlubrano.com)
**Règles:** adaptée d'après la [version pdf de Ludovox](https://ludovox.fr/wp-content/uploads/gravity_forms/48-4e5028f7bc986c64598e09cebc6c12ba/2015/11/regles_elevenses_for_one.pdf)
**page web BGG:** http://boardgamegeek.com/boardgame/158882/elevenses-for-one