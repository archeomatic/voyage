# 6 qui prend !
![6 qui prend : logo](images/6qui/6qui.png)   

**Type :** Jeu de cartes apéro

**Nombre de joueurs :** 2-10

**Durée:** 45 mn

**But :** Récolter le moins possible de têtes de bœufs.

**Gagnant :** Le gagnant est celui ayant comptabilisé le moins de têtes en fin de partie.

**Matériel :** 104 cartes numérotées de 1 à 104.



## I – Mise en place

1. Un crayon et une feuille.
2. Mélanger les cartes.
3. Distribuer **10 cartes** à chaque joueur.
4. Chaque joueur ordonne ses cartes de façon **croissante**.
5. **Formation des 4 rangées :** Déposez les 4 cartes situées sur le dessus de la pile des cartes non  distribuées sur la table, faces visibles. Chaque carte représente le  début d’une série. Cette série ne devra pas comporter plus de 5 cartes  en tout. 
   La pile des cartes restantes ne servira pas pour la manche en cours.

------

## II – Déroulement du jeu

### A - Mise en jeu des cartes

1. Tous les joueurs prennent **une carte** de leur jeu pour la déposer **face cachée** devant eux sur la table.
2. Quand tout le monde a posé, on **retourne** les cartes.
3. Celui qui a déposé la carte la **plus faible** est le **premier à jouer**. Il pose alors sa carte dans la **rangée de son choix**.
4. Puis vient le tour de celui ayant posé la 2ème carte la plus faible jusqu’à ce que tout le monde ait posé. 
   Les cartes d’une série sont toujours déposées les unes à côté des autres.

Répéter ce processus jusqu’à ce que les 10 cartes de chaque joueur soient épuisées.

**Disposition des cartes :**
Chaque carte jouée ne peut convenir qu’à une seule série :

- **Valeurs croissantes :** Les cartes d’une série  doivent toujours se succéder dans l’ordre croissant de leurs valeurs. On pose donc toujours une carte de plus forte valeur que la précédente.
- **La plus petite différence :** Si vous avez le choix entre plusieurs séries : sachez qu’**une carte doit toujours être déposée dans la série où la différence entre la dernière  carte déposée et la nouvelle est la plus faible**.
  *Exemple : Vous avez un 22 : Vous devrez le poser après le 20 (différence de 2) et non après le 17 (différence de 5).*

### B - Encaissement des cartes

- **Série terminée :** **Lorsqu’une série est terminée**  (qu’elle comporte 5 cartes) : Alors, **le joueur qui joue dans l’une de  ces séries doit ramasser les 5 cartes de la série** (**sauf celle qu’il a  posée qui forme le début d’une nouvelle série**).
- **Carte trop faible :** Si un joueur possède **une carte si faible qu’elle ne peut entrer dans  aucune des séries, alors il doit ramasser toutes les cartes d’une série de son choix**. **Sa carte faible représente alors la première carte d’une  nouvelle série**. (La série ramassée sera celle ayant le moins de TdB. Ces têtes sont des points négatifs en fin de partie).

### C - Précisions sur les Têtes de Bœufs

- **Les Têtes de Bœufs (TdB)** sont des points négatifs (le joueur qui en possède le moins gagne la partie).
- Chaque carte, en plus de sa valeur présente un ou plusieurs symboles TdB. Chaque symbole TdB = 1 point négatif.
- Les cartes :
  - Qui finissent par 5 possèdent 2 TdB
  - Qui finissent par 0 possèdent 3 TdB
  - Formant un doublet (11, 22, etc.) possèdent 5 TdB

<img src="images/6qui/55.png" alt="55" style="zoom:25%;" />

Le **nombre 55** est à la fois un **doublet** et un **nombre 5**, cette carte contient donc 7 TdB ! (La pire carte du jeu)

- **Pile de TdB :** Doit être posée devant vous sur la table. Les cartes ramassées iront dans cette pile et NE SONT PAS intégrées à votre main !

### D - Fin de manche

- Lors que les joueurs ont **joué leurs 10 cartes** (ils n’ont plus de cartes en main), la manche prend fin.
- Chaque joueur **compte alors ses points négatifs** dans sa pile de TdB.
- On **note le résultat** de chaque joueur sur une **feuille de papier** et on commence une nouvelle manche.
- On joue plusieurs manches jusqu’à ce que l’un des joueurs ait réuni en tout plus de **66 têtes de bœuf**. Le vainqueur de la partie est alors le joueur qui a le moins de têtes  de bœuf. Avant le début du jeu, il est bien sûr possible de convenir  d’un autre total de points ou d’un nombre de manches maximum.

## III – Sources 

**Règle du jeu :** https://www.regledujeu.fr/6-qui-prend/

**Auteur :** Wolfgang Kramer

**Version pnp: **personnelle; créée avec la fonction Atlas de QGIS .
